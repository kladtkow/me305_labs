'''
@file mainpage.py

@mainpage
@section sec_intro Introduction
@details This website includes detailed documentation for all of the projects I completed in ME 305.
@author Kyle Ladtkow
@date September 22, 2020 through October 20, 2020

@page lab01 Lab 01: Fibonacci Number Generator
@section sec_problem1 Problem Statement
@brief Create a Fibonacci number generator for an index n
@details In this project, the goal was to create a callable function in Python
that, when called with an index, would output the corresponding Fibonacci 
number; the indices had to be an integer greater than or equal to 0. The
function had to be able to calculate Fibonacci numbers indexed from 1 to 100
fairly quickly (within 1 second) and it had to be able to calculate numbers
indexed at around 1000 within a reasonable amount of time. 

The full problem statement can be found in the linked PDF:
@link https://canvas.calpoly.edu/courses/32683/files/1665386/download?download_frd=1  
@endlink

@section documentation1 Code Documentation
@details The following link will take you to the main file page for this lab. 
This page contains the Fibonacci generator function and its relevant documentation. 
@link lab01_main.py
@endlink

@section sourcecode1 Source Code
@details The following link will take you to the main Python file for this lab:
@link 


'''