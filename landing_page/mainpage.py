'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
@details This website includes detailed documentation for all of the projects I completed in ME 305 and ME 405. For more detais and documentation, click one of the links below to view the documentation.
@author Kyle Ladtkow
@date September 22, 2020 through January 5, 2021

@section sec_me305 ME 305
@details kladtkow.bitbucket.io/me305
@endlink

@section sec_me405 ME 405
@details kladtkow.bitbucket.io/me405
@endlink
'''