'''
@file lab2b_MAIN.py

@brief  A file containing code to create objects and simulate a virtual LED
        and control an LED on the Nucleo L476RG Development Board. 
@details    This file is the main command file for two simulations. The first
            is a virtual LED which prints "on" and "off" into the console on
            repeat. The second is a controller for an LED on a microcontroller.
            Specifically, this controller adjusts the brightness of the LD2 
            LED on the Nucleo L476RG Development Board based on a sawtooth 
            wave of a certain period. If left alone, the console will 
            repeatedly print "on" and "off" while the LD2 LED will brighten
            and dim in a sawtooth pattern.
'''

from lab2b_FSM import VirtualLED, taskLEDPattern, taskSawDimmer, mcuLED

## LED State Object
LED_State = VirtualLED()

## Microcontroller LED State Object
mcuLEDState = mcuLED()

## Defines a constant for the interval of time between state changes
interval = 1 #Interval of time in seconds

## Defines a period for the sawtooth function used for the MCU LED, must be an integer
period = 10 #Period of time in seconds

## Task Objects
task1 = taskLEDPattern(interval, LED_State)
task2 = taskSawDimmer(period, mcuLEDState)

## Run the task objects by calling task#.run() repeatedly
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()