'''
@file       lab3_main.py

@brief      Main program runs both the encoder, the user interface, and the
            comms task round-robin.

@details    This file is the main command file for the encoder. Overall, this
            task tree takes user input in the form of a keypress and returns 
            a certain value. It establishes task objects for the user 
            interface, the encoder, and the comms. It also creates an object 
            for the encoder drivers to be used in the encoder task. It runs 
            the encoder and interface tasks on repeat for specified intervals 
            in a "round-robin" style, using the comms task to communicate 
            information and commands between.

@author     Kyle Ladtkow

@date       October 20, 2020
'''
 
from lab3_encoder import TaskEncoder, EncoderDriver
from lab3_interface import TaskUI

encoder_interval = (32768/11200)*(10**6)

## User Interval variable, the time between checks 
user_interval = 0.01*(10**6)

## Encoder object, keeps track of position and delta
encoder = EncoderDriver()

## User interface task, works with serial port
task0 = TaskUI(0, user_interval, dbg=False)

## Encoder task, tracks and alters the position of the motor encoder
task1 = TaskEncoder(1, encoder_interval, encoder, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()