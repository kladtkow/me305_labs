
"""
@file       lab3_encoder.py

@brief      A task and object that tracks and alters the position of a motor 
            encoder.

@details    This task uses the Nucleo Development L47RG to interact with a 
            motor's encoder. Specifically, it tracks the position and position
            delta of the motor's encoder. It can also zero the position of the
            encoder. It does any of these three tasks after receiving the
            correct ASCII integer value from the interface task.

@n          This file also contains the encoder driver class. This class 
            creates the tools that allow the task to constantly update the
            position, obtain the position, obtain the position delta, and zero
            the position of the encoder.
            
@author     Kyle Ladtkow

@date       October 20, 2020
"""

import lab3_comms
import utime
import pyb

class TaskEncoder:
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    def __init__(self, taskNum, encoder_interval, encoder, dbg=True):
        '''
        @brief Creates an encoder task object.
        
        @param taskNum A number to identify the task
        
        @param encoder_interval An integer number of microseconds between runs
        of the task. This interval should be calculated so that now encoder 
        ticks are missed.
        
        @param encoder An encoder drive object to be used in the task.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(encoder_interval)
        
        self.encoder = encoder
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created encoder task')
            
    def run(self):
        '''
        @brief      Runs one iteration of the task
        
        @details    For the specified interval, this task will repeat. It runs
                    through two states, one that is only run at task startup,
                    and one that is run on repeat until given a command by
                    the interface task. 
        
        @n          If the command from the interface corresponds to the ASCII
                    value for 'z', the task will zero the position of the 
                    encoder.
                    
        @n          If the command from the interface corresponds to the ASCII
                    value for 'p', the task will return the position of the 
                    encoder.
                    
        @n          If the command from the interface corresponds to the ASCII
                    value for 'd', the task will return the previous position 
                    delta of the encoder.        
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printDebug()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printDebug()
                self.encoder.update()
                
                # Run State 1 Code
                if lab3_comms.cmd:
                    if (lab3_comms.cmd==122):
                        lab3_comms.resp = self.encoder.zeroPosition()
                        lab3_comms.cmd = None
                    elif(lab3_comms.cmd==112):
                        lab3_comms.resp = self.encoder.getPosition()
                        lab3_comms.cmd = None
                    elif(lab3_comms.cmd==100):
                        lab3_comms.resp = self.encoder.getDelta()
                        lab3_comms.cmd = None
                    else:
                        lab3_comms.resp = 1
                        lab3_comms.cmd = None
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if
                    the debug variable is set.
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

class EncoderDriver:
    def __init__(self):
        '''
        @brief Creates an encoder drivers object.
        '''
        self.tim = pyb.Timer(3)
        self.tim.init(prescaler=0,period=0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        self.oldposition = 0
        self.newposition = 0
        self.position = 0
        self.zeroOffset = 0
        self.storedDelta = 0
    
    def update(self):
        '''
        @brief      Updates the position variable to reflect the real position.
        
        @details    This function probes the encoder to find the position. It
                    finds the position delta between the previous position
                    and the newest value. It then checks if the delta is 
                    "good" or "bad" and adjusts it if it is bad. It then adds
                    the delta to the previous position and updates that value
                    to be the current position. It also stores the delta if
                    it is not 0.
        '''
        self.tick1 = self.position
        self.tick2 = self.tim.counter()-self.zeroOffset
        self.delta = self.tick2-self.tick1
       
        if(abs(self.delta)>((0xFFFF+1)/2)):
            if(self.delta>0):
                self.delta -= 0xFFFF+1
            elif(self.delta<0):
                self.delta += 0xFFFF+1
            else:
                print('ERROR')
        elif(abs(self.delta)<=((0xFFFF+1)/2)):
            pass
        else:
            print('ERROR')
        self.position += self.delta
        
        if(self.delta != 0):
            self.storedDelta = self.delta
        else:
            pass
        
    def getDelta(self):
        '''
        @brief      Returns the previous non-zero delta.
        '''
        return 'The delta is: ' + str(self.storedDelta)
    
    def getPosition(self):
        '''
        @brief      Returns the current position.
        '''
        return 'The position is: ' + str(self.position)
    
    def zeroPosition(self):
        '''
        @brief      Resets the position to 0.
        '''
        self.zeroOffset = self.tim.counter()
        self.position = 0
        return 'The position is now: ' + str(self.position)
    