'''
@file lab01_main.py
@brief  A Python script containing a function to find a specific Fibonacci number.
@details The function requires an input from the user in the form of a 
non-negative integer. Once input, the function calculates the requested
Fibonacci number using a loop.
@author Kyle Ladtkow
@date September 28, 2020
'''
def fib(idx):
    '''
    @brief A function to find the Fibonacci number of a certain index
    @details The function requires an index number from the user. The function check 
    if that input is an integer before passing it on. The function then check
    what value the integer is, then calculates the Fibonacci number of that
    integer and prints it to the console.
    @param idx An integer index for the desired Fibonacci number.
    '''
    if(type(idx) is int):
        if(idx == 0):  #  This conditional statement checks to see if the input index is 0. If it is 0, then it will display the 0th Fibonacci number, which is one of the starter conditions to the sequence.
            fib_num = 0 # Creates a variable for the Fibonacci number and defines it for the 0th term.
            print('Calculating Fibonnacci number at '
              'index n = {:}.'.format(idx)) # Prints a message telling the user it is calculating the number.
            print('The Fibonnacci number is: {:}'.format(fib_num)) # Prints a message telling the user the desired Fibonacci number.
        elif(idx == 1): # Checks if the index number is 1
            fib_num = 1 # Defines the 1st Fibonacci number as 1
            print('Calculating Fibonnacci number at '
              'index n = {:}.'.format(idx)) 
            print('The Fibonnacci number is: {:}'.format(fib_num))
        elif(idx >= 2): # Checks if the index number is 2 or greater
            fib_values = [0,1] # Defines the 0th and 1st Fibonacci numbers in an array
            for x in range(2,idx+1): # Repeatedly calculates Fibonacci values until the desired index is achieved
                fib_values.append(0) # Adds a placeholder slot to the Fibonacci array
                fib_values[x] = fib_values[x-1]+fib_values[x-2] # Calculates the next Fibonacci number and puts it in the empty slot
            fib_num = fib_values[idx] # Defines the desired Fibonacci value as the last slot value in the fib_values array
            print('Calculating Fibonnacci number at '
              'index n = {:}.'.format(idx))
            print('The Fibonnacci number is: {:}'.format(fib_num))
        else:
            print('The index number needs to be greater than or equal to 0')
    else:
        print('No offense but that is not an index number.'
              'An index number is an integer above or equal to 0'
              'Try again.')
