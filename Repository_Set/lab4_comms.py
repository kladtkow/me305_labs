"""
@file       lab4_comms.py

@brief      A semi-global file communication file.

@details    This file acts as a "comms" system between the encoder and the
            interface task. It stores the command from the interface and 
            passes it to the encoder. It also stores the response from the 
            encoder and passes it to the interface. For this lab, it also
            stores data from the data collection task and allows the data user
            interface task to take that data and send it off.

@author     Kyle Ladtkow

@date       November 3, 2020
"""
import array

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None

## Data list from data gen task
data = array.array('i',2*[0])

