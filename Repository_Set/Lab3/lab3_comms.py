"""
@file lab3_comms.py

@brief      A semi-global file communication file.

@details    This file acts as a "comms" system between the encoder and the
            interface task. It stores the command from the interface and 
            passes it to the encoder. It also stores the response from the 
            encoder and passes it to the interface.

@author     Kyle Ladtkow

@date       October 20, 2020
"""

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None
