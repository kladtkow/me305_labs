'''
@file HW0x00_MAIN_ME305.py

@brief A file containing code to create objects and simulate two elevators simultaneously.
@details This file is the main command file for the elevator simulation. It 
creates several objects for the buttons, motor, floor sensors and for the 
instance of the elevator. It then runs two simultaneous simulations of 
elevators for virtually forever.
'''

from HW0x00_FSM_ME305 import Button, MotorDriver, TaskElevator, ElevatorPosition, elevatorInstance
        
## Motor Object
motor = MotorDriver()

## Button Object for "Floor 1 Button"
button_1 = Button()

## Button Object for "Floor 2 Button"
button_2 = Button()

## Button Object for "Floor 1 Sensor"
first = ElevatorPosition()

## Button Object for "Floor 2 Sensor"
second = ElevatorPosition()

## Instance Object, aka which elevator are we talking about
Instance = elevatorInstance()

## Task object
task1 = TaskElevator(0.1, motor, button_1, button_2, first, second, 1) # Will also run constructor
task2 = TaskElevator(0.1, motor, button_1, button_2, first, second, 2) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()