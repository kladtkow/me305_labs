'''
@file lab2b_FSM.py

@brief A file containing classes and objects used to simulate a virtual LED 
            and to vary the brightness of an MCU's LED
@details    This file serves as the finite-state-machine used to simulate a
            virtual LED and interact with the LD2 LED on the Nucleo L476RG
            Development Board. The Virtual LED in this case is represented
            by a console printout of the LED's state (on or off) and "blinks"
            on and off with a given interval. This machine uses a sawtooth 
            wave of a given period to adjust the brightness of the LD2 LED;
            it starts from 0% brightness at the beginning of the period and 
            linearly increases to 100% brightness at the end of the period.
'''

import pyb

class taskLEDPattern:
    '''
    @brief      A finite state machine to simulate a virtual LED.
    @details    This class implements a finite-state-machine to simulate the
                on and off blinking of a virtual LED. Using a given interval
                value, it cycles and reports the state of the LED for however
                long the task is run for.
    '''
    
    def __init__(self, interval, LED_State):
        '''
        @brief      Creates a taskLEDPattern object. 
        @details    This class's initialization includes the interval in 
                    between state changes, and the LED state object.
        '''
        self.LED_State = LED_State
        
        self.interval = interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the LED pattern task.
        @details    This command runs the task between two states: on and off.
                    The LED will turn on at first and print its state. It will
                    then wait the amount of time prescribed by the interval.
                    Then, the LED will turn off and print its state. It will
                    then wait the interval time again.
        '''
        if(type(self.interval) is int or type(self.interval) is float):
            self.LED_State.LEDOn()
            print('LED is: ' + str(self.LED_State.state))
                
            pyb.delay(self.interval*1000)
                
            self.LED_State.LEDOff()
            print('LED is: ' + str(self.LED_State.state))

            pyb.delay(self.interval*1000)
        else:
            print('ERROR! INPUT INTERVAL IS NOT AN INT OR FLOAT VALUE!')                

class VirtualLED:
    '''
        @brief      A virtual LED class.
        @details    This class represents a simulated virtual LED. It can
                    either be "on" or "off".
    '''
    def __init__(self):
        '''
        @brief      Creates a virtual LED object. 
        '''
        pass
    
    def LEDOn(self):
        '''
        @brief      Defines the virtual LED as "on"
        '''
        self.state = 'On'
    
    def LEDOff(self):
        '''
        @brief      Defines the virtual LED as "off"
        '''
        self.state = 'Off'

class mcuLED:
    '''
    @brief      A microcontroller LED class.
    @details    This class represents the state of the LD2 LED on the Nucleo
                L476RG Development Board, specifically its brightness.
                The brightness of this LED can be between 0% and 100%.
    '''
    def __init__(self):
        '''
        @brief      Creates a microcontroller LED object. 
        @details    This initialization binds a timer channel to Pin A5 (the 
                    LD2 pin) so that its brightness can be controlled by 
                    pulse-width-modulation (PWM).
        '''
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
    
    def LEDBright(self, level):
        '''
        @brief      Defines the LED's brightness at a certain percent level.
        '''
        self.t2ch1.pulse_width_percent(level)
    
    def LEDClear(self):
        '''
        @brief      Defines the LED's brightness as 0 (off).
        '''
        self.t2ch1.pulse_width_percent(0)

class taskSawDimmer:
    '''
    @brief      A finite state machine to control an LED with a sawtooth wave.
    @details    This class implements a finite-state-machine to control the
                brightness of the LD2 LED on the Nucleo L476RG Development 
                Board. The brightness level is dictated by a sawtooth wave
                within a given period.
    '''
    def __init__(self, period, mcuLEDState):
        '''
        @brief      Creates a taskSawDimmer object. 
        @details    This class's initialization includes the given period for
                    the sawtooth wave and the microcontroller LED state 
                    object.
        '''
        self.period = period
        
        self.mcuLEDState = mcuLEDState
        
    def run(self):
        '''
        @brief      Runs one iteration of the sawtooth LED dimmer task.
        @details    This command runs the task for 100 percent levels. It
                    starts at a brightness of 0% and increments by 1% until
                    the end of the period, where it will be 100%. At the end
                    of the period, it will go back to 0%.
        '''
        if(type(self.period) is int):
            for idx in range(1, 101):
                self.mcuLEDState.LEDBright(idx)
                pyb.delay((int(self.period)*10))
                self.mcuLEDState.LEDClear()
        else:
            print('ERROR! INPUT INTERVAL IS NOT AN INT OR FLOAT VALUE!')
    