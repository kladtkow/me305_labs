"""
@file       lab5_interface.py

@brief      A task that tracks serial input from the phone app and sends
            relevant information to the blinking LED task.

@details    This task uses the UART class to track any commands from the 
            phone app. The way the phone app is structured, the desired 
            frequency is sent to our HM-11 Bluetooth Low Energy Module, which
            directly inputs into the serial port. This task decodes that serial
            input into the desired frequency (filtering out any non-integer
            values) and then sends that frequency to the blinking LED task.
            Once the LED task has begun blinking, it will signal for this task
            to begin searching for another command.
            
@author     Kyle Ladtkow

@date       November 10, 2020
"""

import lab5_comms
from pyb import UART
import utime

class TaskUI:
    ## State 0: Initialization State
    S0_INIT             = 0
    
    ## State 1: Waiting for Character State
    S1_WAIT_FOR_FREQ    = 1
    
    ## State 2: Waiting for Response State
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, taskNum, user_interval, dbg=True):
        '''
        @brief Creates an interface task object.
        
        @param taskNum A number to identify the task
        
        @param user_interval An integer number of microseconds between runs
        of the task. This interval decides how often the serial is checked.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
        
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(user_interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(3, 9600)
        
        self.read = 0
        
        self.int_str = ''
        
        if self.dbg:
            print('Created user interface task')
            
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        
        @details    For the specified interval, this task will repeat. It runs
                    through three states. State 0 is only run on task
                    startup and immediately moves to State 1. State 1 
                    continuously checks the serial to see if the user has
                    typed anything into the REPL. If the user has typed 
                    a relevant frequency, it will send that value to the
                    LED task and move to State 2. In State 2, the task 
                    waits for a response from the LED task. From the
                    response, it will output a response relating to the current
                    LED frequency.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.printDebug()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_FREQ)
            
            elif(self.state == self.S1_WAIT_FOR_FREQ):
                self.printDebug()
                
                # Run State 1 Code
                if self.ser.any():
                    self.read = str(self.ser.readline())
                    self.int_str = ''
                    for i in range(0, len(self.read)):
                        if(self.read[i] == '1' or self.read[i] == '2' or self.read[i] == '3' or self.read[i] == '4' or self.read[i] == '5' or self.read[i] == '6' or self.read[i] == '7' or self.read[i] == '8' or self.read[i] == '9' or self.read[i] == '0'):
                            self.int_str += self.read[i]
                        elif(self.read[i] == 'b' or self.read[i] == "'" or self.read[i] == "\\" or self.read[i] == 'x'):
                            pass
                        else:
                            print('ERROR! FREQUENCY MUST BE AN INTEGER')
                            break        
                    if (self.int_str != ''):
                        lab5_comms.cmd = int(self.int_str)
                    else:
                        lab5_comms.cmd = None
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                else:
                    pass
           
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printDebug()
                
                # Run State 2 Code
                if (lab5_comms.resp != 0 and lab5_comms.resp != None):
                    print('The LED is blinking at: ' + str(lab5_comms.resp) + ' Hz.')
                    lab5_comms.resp = None
                    self.transitionTo(self.S1_WAIT_FOR_FREQ)
                
                elif (lab5_comms.resp == 0):
                    print('The LED has stopped blinking.')
                    lab5_comms.resp = None
                    self.transitionTo(self.S1_WAIT_FOR_FREQ)
                
                else:
                    pass
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if 
                    the debug variable is set.
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)