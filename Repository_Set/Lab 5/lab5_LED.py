
"""
@file       lab5_LED.py

@brief      A task and object that control the on and off blinking of an LED

@details    This task blinks the LED on a Nucleo Development L47RG board on
            and off with a given frequency. That frequency is given by the 
            user interface task. Once this task has begun blinking, it will
            signal the user interface task to look for further commands. Even
            during its blinking, its frequency can be changed if the interface
            task commands it to.

@n          This task makes use of an LED object contained within this script.
            This object contains the function for toggling the LED's state, as
            well as initializing the LED's pin.
            
@author     Kyle Ladtkow

@date       November 10, 2020
"""

import lab5_comms
import utime
import pyb

class TaskBlinker:
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    S2_BLINKER          = 2
    
    def __init__(self, taskNum, input_interval, mcuLED, dbg=True):
        '''
        @brief Creates a blinking LED task object.
        
        @param taskNum A number to identify the task
    
        @param input_interval The interval in microseconds between state checks.
        
        @param mcuLED An LED driver object to be used in the task.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(input_interval)
        
        self.mcuLED = mcuLED
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        self.freq = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created encoder task')
            
    def run(self):
        '''
        @brief      Runs one iteration of the task
        
        @details    For the specified interval, this task will repeat. It runs
                    through three states. State 0 is only run on startup and
                    immediately transitions to State 1. State 1 has the task 
                    waiting for a command from the interface task. If a command
                    is received, the task will transition to State 2 and send
                    a response back to the user interface task. If another command
                    is received, the task will briefly switch back to State 1
                    and then back to State 2. Otherwise, this task will stay in
                    State 2, blinking at the commanded frequency, in perpetuity.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printDebug()
                
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printDebug()
                
                # Run State 1 Code
                if lab5_comms.cmd:
                    self.per_half = int((10**6)/(2*lab5_comms.cmd))
                    self.nextToggle = utime.ticks_add(self.curr_time, self.per_half)
                    lab5_comms.resp = lab5_comms.cmd
                    lab5_comms.cmd = None
                    self.transitionTo(self.S2_BLINKER)
                
                elif lab5_comms.cmd == 0:
                    lab5_comms.cmd = None
                    lab5_comms.resp = 0
                else:
                    pass
                
            elif(self.state == self.S2_BLINKER):
                self.printDebug()
                if (utime.ticks_diff(self.curr_time, self.nextToggle) >= 0):
                    self.mcuLED.toggle(self.mcuLED.LEDState)
                    self.nextToggle = utime.ticks_add(self.curr_time, self.per_half)
                    if lab5_comms.cmd != None:
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                    else:
                        pass
                else:
                    pass
            else:
                # Invalid state code (error handling)
                pass
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        else:
            pass
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if
                    the debug variable is set.
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

class LEDDriver:
    def __init__(self):
        '''
        @brief Creates an LED drivers object.
        '''
        self.LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.LEDState = 0
    
    def toggle(self, LEDState):
        '''
        @brief Toggles the LED from on to off or off to on, depending on the current state
        '''
        if LEDState == 0:
            self.LED.high()
            self.LEDState = 1
        
        elif LEDState == 1:
            self.LED.low()
            self.LEDState = 0
        
        else:
            print('ERROR')