"""
@file       lab6_FrontUI.py

@brief      A mainscript that takes input from the user, collects data from
            the Nucleo, and plots that data.

@details    This script serves as the communication between the computer and
            the Nucleo. When run, it asks the user for input. If the input is
            the letter 'k', it will then prompt the user for a k-value between
            0 and 1. If one is given, it will pass that k-value to the Nucleo
            to intiate the step response. It will then wait for position data
            to come back and graph it.

@n          Because the communication between the PC and the Nucleo is through
            the serial port, the way data is sent through is via a string.
            The Nucleo encodes the data into a string with specific stop
            characters. This script reads that string and, using those same
            stop characters, it determines which data is time (seconds) and 
            which is angular speed (rpm).

@author     Kyle Ladtkow

@date       November 24, 2020
"""
import serial
import matplotlib.pyplot as plt
import numpy as np

ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)

inv = ''

time = [0]

speed = [0]

dataStop = 0

endScript = 0

sent_cmd = 0

data = None


def sendChar():
    '''
    @brief      Sends a command to the Nucleo.
        
    @details    This function will query the user for a character. If the 
                character is a 'k', it will then query the user for a k-value.
                It will then send those two inputs to the Nucleo.
    '''
    
    inv = input('Give me a character: ')
    
    if(inv == 'k'):
        k_value = input('Give your new k value as a number between 0 and 1: ')
        if(float(k_value) >= 0 and float(k_value) <= 1):
            inv += k_value
            ser.write(str(inv).encode('ascii'))
        else:
            print('ERROR: K VALUE MUST BE FLOAT BETWEEN 0 AND 1')
    else:
        print('ERROR: COMMAND MUST BE K')
    
def readSerial():
    '''
    @brief      Reads the serial port for data.
        
    @details    This function checks the serial port for if any data has been
                sent by the Nucleo. If data has been sent by the Nucleo in the
                form of a string, it will return that data. If the serial port
                still has a residual 'k' or k-value, it will remove it.
    '''
    read = ser.readline().decode('ascii')
    if(read.count('k') != 0):
        read = '' 
    else:
        return read

while (endScript != 1):
    if (dataStop != 1):
        if (sent_cmd == 0):
            sendChar()
            sent_cmd = 1
        else:
            pass
        
        data = readSerial()        
        
        if (data != '' and data != None):
            p = 0
            n = 0
            while (p != data.count('&')-1):
                phtime = ''
                phspeed = ''
                while (data[n] != ','):
                    phtime += data[n]
                    n += 1
                n += 1
                time.append(int(phtime)/(1*(10**6)))
                phtime = ''
                while (data[n] != '&'):
                    phspeed += data[n]
                    n += 1
                n += 1
                p += 1
                speed.append(int(phspeed))
                phspeed = ''
            dataStop = 1
    elif (dataStop == 1):
        fig, ax = plt.subplots()
        ax.plot(time,speed)
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Angular Speed [rpm]')
        ax.set_title("Angular Speed of the Encoder vs Time")
        ser.close()
        endScript = 1
        pass
    else:
        pass