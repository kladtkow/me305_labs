"""
@file       lab6_comms.py

@brief      A semi-global file communication file.

@details    This file acts as a "comms" system between the encoder and the
            interface task. It stores the command from the interface and 
            passes it to the encoder. It also stores the response from the 
            encoder and passes it to the interface. For this lab, it also
            stores data from the data collection task and allows the data user
            interface task to take that data and send it off.

@author     Kyle Ladtkow

@date       November 24, 2020
"""

import array
## The command character sent from the user interface task to the motor controller task
cmd     = None

## The response from the motor controller task after performing
resp    = None

## Data list from data gen task
data = array.array('i',2*[0])