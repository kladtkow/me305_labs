"""
@file       lab6_controller.py

@brief      The Python file containing a task that controls the motor, encoder,
            and the proportional gain controller. It also contains the drivers
            for those three devices as well.

@details    The task in this file is meant to control the motor for this task.
            It contains a motor driver, which can modulate the PWM level at
            will. It also contains an encoder driver, which can determine what
            position and what speed the motor output shaft is at. It also 
            contains a closed-loop task, which checks what speed the output
            shaft is running at and adjusts the PWM based on a proportional
            gain value.
            
@author     Kyle Ladtkow

@date       November 24, 2020
"""
import lab6_comms
import pyb
import utime
import math
import array

class TaskMotorController:
    '''
    @brief  A task that generates a step response using a motor, encoder, and
            closed-loop proportional gain controller.
    @details    This task runs a finite-state-machine that cycles between
            three states. State 0 is only run upon initialization and immediately
            transitions to State 1. In State 1, the task awaits a command from
            the Nucleo UI task, specifically in the form of a "k" plus a gain
            value. Once it receives that, it transitions to State 2, where it
            modulates the speed of the motor until it reaches within plus or
            minus 10rpm of the target speed.
    '''
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    ## Drive motor state
    S2_DRIVE_MTR     = 2
    
    def __init__(self, taskNum, encoder_interval, sample_interval, ClosedLoop, Encoder, Motor, dbg=True):
        '''
        @brief Creates a motor controller object
        @param taskNum The number task that is running on main.py
        @param encoder_interval The amount of time between encoder updates so
                                as to not miss any ticks
        @param sample_interval The amount of time between checks of the motor's
                                speed
        @param ClosedLoop A proportional gain controller object
        @param Encoder An encoder object tied to the motor
        @param Motor A motor object
        @param dbg A variable denoting whether or not the debug message should
                    print
                
        '''
        ## The name of the task
        self.taskNum = taskNum
        
        ## The user-given encoder sample-rate
        self.interval = int(encoder_interval*(10**6))
        
        self.sample_interval = int(sample_interval*(10**6))
        
        ## The local Closed Loop object
        self.ClosedLoop = ClosedLoop
        
        ## The local Encoder Driver object
        self.encoder = Encoder
        
        ## The local Motor Driver object
        self.motor = Motor
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## Initializes the variable representing when to next sample for data collection
        self.next_sample = 0
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.correctedDuty = 0

        if self.dbg:
            print('Created motor controller task')
        
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        
        @details    For the specified interval, this task will repeat. It runs
                    through three states. State 0 is only run on task
                    startup and immediately moves to State 1. State 1 
                    continuously checks for a command from the Nucleo UI task. 
                    If a command is received, it will take the gain value from
                    that command and plug it into the motor, as well as 
                    transitioning to State 2. In State 2, it constantly updates
                    the encoder and checks the speed of the motor. It adds the 
                    speed of the motor and its corresponding timestamp to the 
                    data collection file. With the speed of the motor, it 
                    applies a proportional gain and applies a new PWM level to 
                    the motor. It does this continuously until the speed is 
                    within plus or minus 10rpm of the desired speed.
        
        @n          If the speed comes within the desired range, it will stop
                    collecting data, set the speed to 0, and send the collected
                    data to the Nucleo UI task.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
       
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printDebug()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printDebug()
                self.encoder.update()
                
                # Run State 1 Code
        
                if lab6_comms.cmd:
                    
                    lab6_comms.cmd = lab6_comms.cmd.decode('ascii')
                    
                    if(lab6_comms.cmd[0] == 'k'):
                        self.new_Kp = ''
                        for n in range(1,len(lab6_comms.cmd)):
                            if lab6_comms.cmd[n] == '1' or lab6_comms.cmd[n] == '2' or lab6_comms.cmd[n] == '3' or lab6_comms.cmd[n] == '4' or lab6_comms.cmd[n] == '5' or lab6_comms.cmd[n] == '6' or lab6_comms.cmd[n] == '7' or lab6_comms.cmd[n] == '8' or lab6_comms.cmd[n] == '9' or lab6_comms.cmd[n] == '0' or lab6_comms.cmd[n] == '.':
                                self.new_Kp += lab6_comms.cmd[n]
                            else:
                                pass
                        self.new_Kp = float(self.new_Kp)
                        self.ClosedLoop.set_Kp(self.new_Kp)
                        self.startTime = self.curr_time
                        self.next_sample = utime.ticks_add(self.startTime, self.sample_interval)
                        lab6_comms.data = array.array('i',2*[0])
                        lab6_comms.cmd = None
                        self.transitionTo(self.S2_DRIVE_MTR)
                    
                    else:
                        lab6_comms.resp = 1
                        lab6_comms.cmd = None
            
            elif(self.state == self.S2_DRIVE_MTR):
                self.encoder.update()
                if (utime.ticks_diff(self.curr_time, self.next_sample) >= 0):
                    self.encoder.collectSpeedData(self.startTime, lab6_comms.data)
                    self.next_sample = utime.ticks_add(self.curr_time, self.sample_interval)
                    self.correctedDuty = self.ClosedLoop.update(self.encoder.getSpeed())
                    self.motor.set_duty(self.correctedDuty)
                                        
                    if ((self.encoder.getSpeed() <= (self.ClosedLoop.omega_ref+10)) and (self.encoder.getSpeed() >= (self.ClosedLoop.omega_ref-10))):
                        lab6_comms.resp = 2
                        self.motor.set_duty(0)
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                   
                    else:
                        pass
               
                else:
                    pass
           
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if
                    the debug variable is set.
        '''
        if self.dbg:
            string = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(string)
class ClosedLoop:
    '''
    @brief  A controller that determines what PWM level needs to be applied
            to the motor to get to the desired speed.
    @details    This controller takes rotational speed in and compares it to
            the desired speed. It applies a proportional gain value to their
            difference and adds that to the current PWM level.
    '''
    def __init__(self, k_init, omega_ref):
        '''
        @brief  Creates a closed loop controller object
        @param k_init The initial k value for this controller
        @param omega_ref The desired speed for the motor
        '''
        self.K_p = k_init
        self.omega_ref = omega_ref
        self.duty = 0
        self.newDuty = 0
        
    def update(self, omega):
        '''
        @brief Compares the current speed to the desired speed and adjusts it
                accordingly.
        @details This function subtracts the current speed from the desired speed
                and multiplies that difference by the proportional gain. That
                new value is added to the current PWM level and sent to the 
                main task.
        @n      Because of torque and PWM limitations with these motors, the 
                absolute value of the PWM can not be below 30 or above 100. If
                it is below 30, the motor will not have enough power to overcome
                the frictional torque of the motor; therefore, this function will
                adjust offending duties to just 30 in that direction. It can 
                not go above 100, as the level is a percentage of PWM; 
                therefore, this function will adjust the offending duties to 
                100 in that direction.
        '''
        self.duty += self.K_p*(self.omega_ref-omega)
        if (self.duty > 100):
            self.newDuty = 100
        elif (self.duty < -100):
            self.newDuty = -100
        elif (self.duty < 30 and self.duty > 0):
            self.newDuty = 30
        elif (self.duty > -30 and self.duty < 0):
            self.newDuty = -30
        else:
            self.newDuty = self.duty
        return int(self.newDuty)
    
    def get_Kp(self):
        '''
        @brief Returns the current proportional gain value
        '''
        return self.K_p
    
    def set_Kp(self, new_Kp):
        '''
        @brief Changes the current proportional gain value to a new one
        '''
        self.K_p = new_Kp

class EncoderDriver:
    def __init__(self, timer, pinA_ch, pinB_ch, position_units, speed_units):
        '''
        @brief Creates an encoder drivers object.
        @param timer A timer object corresponding to the encoder
        @param pinA_ch A timer channel object for the A pin
        @param pinB_ch A timer channel object for the B pin
        @param position_units A string indicating which units it will output
                              the angle as
        @param speed_units A string indicating which units it will output the
                              the angular speed as
        '''
        self.tim = timer
        self.phaseA = pinA_ch
        self.phaseB = pinB_ch
        
        self.lastCount = 0
        self.position = 0
        self.converter = 360/4000
        
        self.modifier = 1
        
        self.angleposition_prev = 0
        self.angleposition_new = 0
        
        self.timestamp_prev = 0
        self.timestamp_new = 0
        
        self.angularspeed_prev = 0
        self.angularspeed_new = 0
        
        self.zeroOffset = 0
        self.storedDelta = 0
        self.newDelta = 0
        
        self.position_units = position_units
        self.speed_units = speed_units
        
    def update(self):
        '''
        @brief      Updates the position variable to reflect the real position.
        
        @details    This function probes the encoder to find the position. It
                    finds the position delta between the previous position
                    and the newest value. It then checks if the delta is 
                    "good" or "bad" and adjusts it if it is bad. It then adds
                    the delta to the previous position and updates that value
                    to be the current position. It also stores the delta if
                    it is not 0.
        
        @n          In addition, this function will also calculate the position
                    as an angle and the speed as an angular velocity. Depending
                    on the units input into the driver object, it will adjust
                    the units accordingly.
        '''
        self.tick1 = self.lastCount
        self.tick2 = self.tim.counter()-self.zeroOffset
        self.delta = self.tick2-self.tick1
       
        self.newDelta = EncoderDriver.fixDelta(self, self.delta)
        
        self.position += self.newDelta
        
        self.lastCount = self.tick2
        
        self.angleposition_prev = self.angleposition_new
        self.angleposition_new = int(self.position*self.converter)
        
        self.timestamp_prev = self.timestamp_new
        self.timestamp_new = utime.ticks_us()
        
        self.angularspeed_prev = self.angularspeed_new
        self.angularspeed_new = (10**6)*(self.angleposition_new-self.angleposition_prev)/(utime.ticks_diff(self.timestamp_new, self.timestamp_prev))
        
        if(self.delta != 0):
            self.storedDelta = self.newDelta
        else:
            pass
    
    def fixDelta(self, delta):
        '''
        @brief Fixes the encoder delta so overflow doesn't occur
        '''
        if(abs(delta)>((0xFFFF+1)/2)):
            if(delta>0):
                return int(delta - (0xFFFF+1))
            elif(delta<0):
                return int(delta + (0xFFFF+1))
            else:
                return int(delta)
        
        elif(abs(delta)<=((0xFFFF+1)/2)):
            return int(delta)
        
        else:
            return int(delta)
        
    def getDelta(self):
        '''
        @brief      Returns the previous non-zero delta.
        '''
        return 'The delta is: ' + str(self.storedDelta)
    
    def getPosition(self):
        '''
        @brief      Returns the current position.
        '''
        return 'The position is: ' + str(self.position)
    
    def getAngle(self):
        '''
        @brief Returns the angle of the encoder in whatever units were input
                into the driver object
        '''
        if (self.position_units == 'degrees' or self.position_units == 'deg'):
            return self.angleposition_new
        elif (self.position_units == 'radians' or self.position_units == 'rad'):
            return (self.angleposition_new*(math.pi/180))
        elif (self.position_units == 'revolutions' or self.position_units == 'rev'):
            return (self.angleposition_new/360)
        else:
            pass
        
    def getSpeed(self):
        '''
        @brief Returns the angular speed of the encoder in whatever units were input
                into the driver object
        '''
        if (self.speed_units == 'degrees/second' or self.speed_units == 'deg/s'):
            return self.angularspeed_new
        elif (self.speed_units == 'radians/second' or self.speed_units == 'rad/s'):
            return self.angularspeed_new*(math.pi/180)
        elif (self.speed_units == 'revolutions/minute' or self.speed_units == 'rev/min' or self.speed_units == 'rpm'):
            return self.angularspeed_new*(60/360)
        else:
            pass
        
    def zeroPosition(self):
        '''
        @brief      Resets the position to 0.
        '''
        self.zeroOffset = self.tim.counter()
        self.position = 0
        return 'The position is now: ' + str(self.position)
    
    def collectPosData(self, startTime, array):
        '''
        @brief      Appends time and angular position data to an array.
        
        @details    This function takes a given data collection starting time
                    and, using values from the update function, collects an
                    angular position value for a given time; that time is 
                    measured relative to the given starting time.
        '''
        array.append(utime.ticks_diff(self.timestamp_new, startTime))
        array.append(int(self.getAngle()))
    
    def collectSpeedData(self, startTime, array):
        '''
        @brief      Appends time and angular speed data to an array.
        
        @details    This function takes a given data collection starting time
                    and, using values from the update function, collects an
                    angular speed value for a given time; that time is 
                    measured relative to the given starting time.
        '''
        array.append(utime.ticks_diff(self.timestamp_new, startTime))
        array.append(int(self.getSpeed()))

class MotorDriver:
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, motor_number):
        '''
        @brief Initializes the motor driver with given hardware inputs
        @param nSLEEP_pin   The pin object for nSleep
        @param IN1_pin      The pin timer channel object for IN1
        @param IN2_pin      The pin timer channel object for IN2
        @param timer        The timer object used to control pins IN1 and IN2
        '''
        self.nSleep = nSLEEP_pin
        
        self.timer = timer

        self.IN1 = IN1_pin
        
        self.IN2 = IN2_pin
     
        self.motor_number = motor_number
        
        self.motor_state = 0
    
    def enable (self):
        '''
        @brief Turns on the motor through the NSLEEP pin
        '''
        self.nSleep.high()
        self.motor_state = 1
    
    def disable (self):
        '''
        @brief Turns off the motor through the NSLEEP pin
        '''
        self.nSleep.low()
        self.motor_state = 0
    
    def set_duty (self, duty):
        '''
        @brief Sets the motor to a PWM level in either direction
        
        @details This functions takes a percentage value and applies it in PWM
                to the motor. The motor must be enabled for this to work and
                the absolute value of the PWM value must be below 100.
        '''
        if (self.motor_state == 1 and type(duty) == int):
            if (duty > 0):
                self.IN1.pulse_width_percent(duty)
                self.IN2.pulse_width_percent(0)
                #print('Motor ' + str(self.motor_number) + ' is now spinning forward at ' + str(duty) + '% duty')
            elif (duty < 0):
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(abs(duty))
                #print('Motor ' + str(self.motor_number) + ' is now spinning reverse at ' + str(abs(duty)) + '% duty')
            elif (duty == 0):
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(0)
                #print('Motor ' + str(self.motor_number) + ' is now coasting to a halt.')
            elif (duty > 100 or duty < -100):
                print('ERROR! DUTY CYCLE CAN NOT BE GREATER THAN 100% IN EITHER DIRECTION.')
            else:
                pass
        elif (self.motor_state == 0):
            print('ERROR! MOTOR ' + str(self.motor_number) + ' MUST BE ENABLED FIRST!')
        elif (type(duty) != int):
            print('ERROR! INPUT DUTY CYCLE MUST BE AN INTEGER BETWEEN -100 and 100')
        else:
            pass
