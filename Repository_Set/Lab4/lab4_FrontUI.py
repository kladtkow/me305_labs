"""
@file       lab4_FrontUI.py

@brief      A mainscript that takes input from the user, collects data from
            the Nucleo, and plots that data.

@details    This script serves as the communication between the computer and
            the Nucleo. When run, it asks the user for input. If the input is
            the letter 'g', it sends that 'g' to the serial window, where it 
            will be passed into the Nucleo. It will continue querying the user
            until the command 's' is given. Once 's' is input, this script
            will receive data from the Nucleo through the serial port and plot
            that data using MatPlotLib.

@n          Because the communication between the PC and the Nucleo is through
            the serial port, the way data is sent through is via a string.
            The Nucleo encodes the data into a string with specific stop
            characters. This script reads that string and, using those same
            stop characters, it determines which data is time and which is
            position.

@author     Kyle Ladtkow

@date       November 3, 2020
"""
import serial
import matplotlib.pyplot as plt
import numpy as np

ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)

inv = ''

time = [0]

angle = [0]

dataStop = 0

endScript = 0

def sendChar():
    '''
    @brief      Sends a command to the Nucleo.
        
    @details    This function will query the user for a character. It will
                then send that character through the serial port to the nucleo,
                where it will be processed based on whether it is 'g' or 's'.
    '''
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    
def readSerial():
    '''
    @brief      Reads the serial port for data.
        
    @details    This function checks the serial port for if any data has been
                sent by the Nucleo. If data has been sent by the Nucleo in the
                form of a string, it will return that data. If the serial port
                still has a residual 's' or 'g', it will eliminate it before
                passing the data on.
    '''
    read = ser.readline().decode('ascii')
    if (read == 'g'):
        read = read.replace('g','')
        return read
    elif(read == 's'):
        read = read.replace('s','')
    else:
        return read

while (endScript != 1):
    if (dataStop != 1):
        sendChar()
        data = readSerial()
        if (data != ''):
            p = 0
            n = 0
            while (p != data.count('&')-1):
                phtime = ''
                phpos = ''
                while (data[n] != ','):
                    phtime += data[n]
                    n += 1
                n += 1
                time.append(int(phtime)/(1*(10**6)))
                phtime = ''
                while (data[n] != '&'):
                    phpos += data[n]
                    n += 1
                n += 1
                p += 1
                angle.append(int(phpos))
                phpos = ''
            dataStop = 1
    elif (dataStop == 1):
        fig, ax = plt.subplots()
        ax.plot(time,angle)
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Angle of Rotation [degrees]')
        ax.set_title("Angular Position of the Encoder vs Time")
        ser.close()
        endScript = 1
        pass
    else:
        pass