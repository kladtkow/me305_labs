'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
@details This website includes detailed documentation for all of the projects I completed in ME 305.
@author Kyle Ladtkow
@date September 22, 2020 through October 20, 2020


@page lab01 Lab 0x01: Fibonacci Number Generator
@section sec_problem1 Problem Statement
@brief Create a Fibonacci number generator for an index n.
@details In this project, the goal was to create a callable function in Python
that, when called with an index, would output the corresponding Fibonacci 
number; the indices had to be an integer greater than or equal to 0. The
function had to be able to calculate Fibonacci numbers indexed from 1 to 100
fairly quickly (within 1 second) and it had to be able to calculate numbers
indexed at around 1000 within a reasonable amount of time. 

@section documentation1 Code Documentation
@details The following link will take you to the main file page for this lab. 
This page contains the Fibonacci generator function and its relevant documentation. 
@link lab01_main.py
@endlink

@section sourcecode1 Source Code
@details Copy and paste the following link into your browser to access the 
main Python file for this lab:
https://bitbucket.org/kladtkow/me305_labs/src/master/Lab01/lab01_main.py
@endlink


@page hw0x00 HW 0x00: Elevator Simulation

@section sec_problem_hw_0 Problem Statement
@brief Simulate a two-floor elevator with a finite-state-machine.
@details In this project, the goal was to create a set of classes and objects
that when called would simulate the motion of a two-floor elevator. The way it
did this was by tracking the position of the elevator via floor sensors, 
button pushes, and the state of the motor to create a set of states to move
between. 
@n In this particular script, the buttons were pushed randomly, but 
coded so that it would only go up then down then up, etc. In addition, this
project was also meant to demonstrate multitasking, so there are two elevators
with the same code being simulated at the same time. The states, run count,
and time elapsed all display constantly into the command window.

@section documentation_hw_0 Code Documentation
@details The following links will take you to the main file pages for this lab. 
@n The following link contains the documentation
for the finite-state-machine: 
@link HW0x00_FSM_ME305.py
@endlink
@n The following link contains the documentation
for the main file which calls and runs the finite-state-machine:
@link HW0x00_MAIN_ME305.py
@endlink


@section state_diagram_hw0x00 State Transition Diagram
@brief The state transition diagram created to model the finite-state-machine
after.
@image html statetransitionhw0.jpg

@section sourcecodehw0x00 Source Code
@details Copy and paste the following link into your browser to access the 
main finite-state-machine Python file for this lab:
https://bitbucket.org/kladtkow/me305_labs/src/master/Repository_Set/HW0x00_FSM_ME305.py
@endlink
@n Copy and paste the following link into your browser to access the 
main command Python file for this lab:
https://bitbucket.org/kladtkow/me305_labs/src/master/Repository_Set/HW0x00_MAIN_ME305.py
@endlink

@author Kyle Ladtkow
@date October 6, 2020
'''