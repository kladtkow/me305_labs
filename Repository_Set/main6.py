"""
@file       main.py

@brief      The mainscript that cycles between the motor controller and the
            Nucleo user interface tasks.
            
@details    This script constantly runs on the Nucleo, cycling between the
            data generation and the data user interface tasks in a round-robin
            fashion. This script creates these task objects and defines certain
            objects like the data sampling rate, the serial port for the Nucleo,
            the maximum sampling timespan, etc.
            
@author     Kyle Ladtkow

@date       November 24, 2020
"""
from lab6_controller import EncoderDriver, MotorDriver, ClosedLoop, TaskMotorController
from lab6_NucleoUI import TaskNucleoUI
import pyb
from pyb import UART
import utime

## Encoder interval, the time in seconds between encoder updates that will account for all movements.
encoder_interval = (32768/400000)

## Sample Interval, the time in seconds between data collection for the data generation task.
sample_interval = 1

## User Interval variable, the time in seconds between checks. 
user_interval = 0.01

## UART Serial Object, the serial object that the data user interface task will use to communicate with the PC.
myuart = UART(2)

## The inital proportional gain value
k_init = 0

## The desired motor speed
omega_ref = 1650

## To create the encoder driver object:
# The encoder timer object
tim_enc1 = pyb.Timer(4)
tim_enc1.init(prescaler=0,period=0xFFFF)

# The A and B encoder pin objects
pinB6_ch = tim_enc1.channel(1, pin=pyb.Pin.cpu.B6, mode=pyb.Timer.ENC_AB)
pinB7_ch = tim_enc1.channel(2, pin=pyb.Pin.cpu.B7, mode=pyb.Timer.ENC_AB)

# The encoder object with desired units for angle and speed
encoder1 = EncoderDriver(tim_enc1, pinB6_ch, pinB7_ch, 'rev', 'rpm')

## To create the motor driver object:
# The NSLEEP pin object
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)

# The IN3 and IN4 pin objects for the motor
IN3_pin = pyb.Pin(pyb.Pin.cpu.B0)
IN4_pin = pyb.Pin(pyb.Pin.cpu.B1)

# The timer object for the motor
tim_mot1 = pyb.Timer(3, freq=20000)

# The IN3 and IN4 channel objects
IN3_ch = tim_mot1.channel(3, pyb.Timer.PWM, pin=IN3_pin)
IN4_ch = tim_mot1.channel(4, pyb.Timer.PWM, pin=IN4_pin)

# The motor object
motor1 = MotorDriver(nSLEEP_pin, IN3_ch, IN4_ch, tim_mot1, 1)

# Enables the motor
motor1.enable()

# The closed-loop proportional gain controller object
closedloop = ClosedLoop(k_init, omega_ref)

## Data user interface task, allows the Nucleo to use the serial port to communicate with the PC.
task0 = TaskNucleoUI(0, user_interval, myuart, dbg=False)

## Motor controller task, collects angular position versus time.
task1 = TaskMotorController(1, encoder_interval, sample_interval, closedloop, encoder1, motor1, dbg=False)

## The task list contains the tasks to be run "round-robin" style.
taskList = [task0,
            task1]

## Run the tasks "round-robin" style.
while True:
    for task in taskList:
        task.run()