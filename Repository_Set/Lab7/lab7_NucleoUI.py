"""
@file       lab7_NucleoUI.py

@brief      A task that commands the motor controller task and communicates
            with the PC interface from the Nucleo. 

@details    The task in this file is intended to communicate with the PC
            interface task using a common serial port. It constantly checks
            the serial port to see if any characters have come in from the PC
            interface. If a character has come in, it will send it to the 
            data generation task to be processed. If the data generation task
            sends data through the lab7_comms.py file, it will encode that
            data into many strings and send it through the serial port.
            
@author     Kyle Ladtkow

@date       December 4, 2020
"""
import lab7_comms
from pyb import UART
import utime

class TaskNucleoUI:
    '''
    @brief  A task that interfaces with the computer-side of the program
    @details    This task runs a finite-state-machine that cycles between
            three states. State 0 is only run upon initialization and immediately
            transitions to State 1. In State 1, the task checks for any input
            into the serial port, specifically in the form of a "k" plus a gain
            value or a 'g'. Once it receives that, it sends that input to the 
            controller task and transitions to State 2, where it awaits a 
            response from the controller task. If it receives the correct 
            response, it will take the collected speed and time data and send 
            it through the serial port to the computer side.
    '''
    ## State 0: Initialization State
    S0_INIT             = 0
    
    ## State 1: Waiting for Character State
    S1_WAIT_FOR_CHAR    = 1
    
    ## State 2: Waiting for Response State
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, taskNum, user_interval, serport, dbg=True):
        '''
        @brief Creates a Nucleo User Interface task object.
        
        @param taskNum A number to identify the task
        
        @param user_interval An integer number of microseconds between runs
        of the task. This interval decides how often the serial is checked.
        
        @param serport A serial port object using the UART class. This serial
        port object will be used to communicate with the PC interface.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
        
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(user_interval*(10**6))
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = serport
        
        if self.dbg:
            print('Created user interface task')
            
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        
        @details    For the specified interval, this task will repeat. It runs
                    through three states. State 0 is only run on task
                    startup and immediately moves to State 1. State 1 
                    continuously checks the serial to see if the user has
                    typed anything into the PC interface. If the user has 
                    typed anything, it will send the ASCII value of that key 
                    to the encoder task and move to State 2. In State 2, the task 
                    waits for a response from the data generation task. From 
                    the response, it will output some value.
        
        @n          If the data generation task tells this task that it has 
                    collected data, this task will encode that data into 
                    several strings with certain stop characters indicating 
                    what value is what. It will then send those strings
                    through the serial port to the PC interface. The task will
                    then switch back to State 1, awaiting the next command.
        '''
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printDebug()
                
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printDebug()
                
                # Run State 1 Code
                if self.ser.any():
                    lab7_comms.cmd = self.ser.read()
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                else:
                    pass
           
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printDebug()
                
                # Run State 2 Code
                if lab7_comms.resp:
                    if(lab7_comms.resp == 2):
                        i = 0
                        while i < len(lab7_comms.data):
                            printstr = str(lab7_comms.data[i]) + ',' + str(lab7_comms.data[i+1]) + ',' + str(lab7_comms.data[i+2]) + '&'
                            self.ser.write(printstr.encode('ascii'))
                            i += 3
                        
                            
                        lab7_comms.resp = None
                        self.transitionTo(self.S1_WAIT_FOR_CHAR)
                        
                    else:
                        self.transitionTo(self.S1_WAIT_FOR_CHAR)
                        lab7_comms.resp = None
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if 
                    the debug variable is set.
        '''
        if self.dbg:
            string = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(string)