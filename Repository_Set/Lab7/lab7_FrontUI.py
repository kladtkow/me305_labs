"""
@file       lab7_FrontUI.py

@brief      A mainscript that takes input from the user, collects data from
            the Nucleo, and plots that data, all from the computer end.

@details    This script serves as the communication between the computer and
            the Nucleo. When run, it asks the user for input. If the input is
            the letter 'g', it will send that command to the Nucleo. If the
            input is 'k', it will prompt the user for a proportional gain value
            between 0 and 1. Once given, it will send that 'k' and the gain
            to the nucleo. 

@n          Once either command is sent, the script will then 
            wait for position and speed data to come back and graph them. To 
            evaluate how well the new data matches with the given profile, this
            script calculates a performance criterion, J. Once the data has been
            received, this script calculates and outputs the J-value.

@n          Because the communication between the PC and the Nucleo is through
            the serial port, the way data is sent through is via a string.
            The Nucleo encodes the data into a string with specific stop
            characters. This script reads that string and, using those same
            stop characters, it determines which data is time (seconds), which
            data is position (degrees), and which data is angular speed (rpm).
            Once it parses the data, it creates a subplot with two graphs, 
            one representing angular speed versus time and one representing
            angular position versus time. These two graphs are supposed to 
            closely mimic the given profile.

@author     Kyle Ladtkow

@date       December 4, 2020
"""
import serial
import matplotlib.pyplot as plt
import numpy as np

ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)

inv = ''

time = [0]

speed = [0]

position = [0]

dataStop = 0

endScript = 0

sent_cmd = 0

data = None

## The desired motor speed
ref_time = []
ref_speed = []
ref_position = []

ref = open('reference_front.csv');

while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        ref_time.append(float(t))
        ref_speed.append(float(v)*30)
        ref_position.append(float(x)*30)

    
def sendChar():
    '''
    @brief      Sends a command to the Nucleo.
        
    @details    This function will query the user for a character. If the 
                character is a 'k', it will then query the user for a k-value.
                It will then send those two inputs to the Nucleo.
    '''
    
    inv = input('Give me a character: ')
    
    if(inv == 'k'):
        k_value = input('Give your new k value as a number between 0 and 1: ')
        if(float(k_value) >= 0 and float(k_value) <= 1):
            inv += k_value
            ser.write(str(inv).encode('ascii'))
        else:
            print('ERROR: K VALUE MUST BE FLOAT BETWEEN 0 AND 1')
    elif(inv == 'g'):
        ser.write(str(inv).encode('ascii'))
    else:
        print('ERROR: COMMAND MUST BE K OR G')
    
def readSerial():
    '''
    @brief      Reads the serial port for data.
        
    @details    This function checks the serial port for if any data has been
                sent by the Nucleo. If data has been sent by the Nucleo in the
                form of a string, it will return that data. If the serial port
                still has a residual 'k' or k-value, it will remove it.
    '''
    read = ser.readline().decode('ascii')
    if(read.count('k') != 0):
        read = '' 
    else:
        return read

while (endScript != 1):
    if (dataStop != 1):
        if (sent_cmd == 0):
            sendChar()
            sent_cmd = 1
        else:
            pass
        
        data = readSerial()        
        
        if (data != '' and data != None):
            p = 0
            n = 0
            while (p != data.count('&')):
                phtime = ''
                phspeed = ''
                phpos = ''
                while (data[n] != ','):
                    phtime += data[n]
                    n += 1
                n += 1
                time.append(int(phtime)/(1*(10**6)))
                phtime = ''
                
                while (data[n] != ','):
                    phspeed += data[n]
                    n += 1
                n += 1
                speed.append(int(phspeed))
                phspeed = ''
                
                while (data[n] != '&'):
                    phpos += data[n]
                    n += 1
                n += 1
                position.append(int(phpos))
                p += 1
            
            dataStop = 1
    elif (dataStop == 1):
        plt.figure()
        plt.subplot(2,1,1)
        plt.plot(time,speed)
        plt.xlabel('Time [s]')
        plt.ylabel('Angular Speed [rpm]')
        plt.title("Angular Speed and Position of the Encoder vs Time")
        
        plt.subplot(2,1,2)
        plt.plot(time,position)
        plt.xlabel('Time [s]')
        plt.ylabel('Angular Position [deg]')
        ser.close()
        endScript = 1
        print('Plot is now available!')
        
        J_list = []
        J = 0
        omega_diff = 0
        angle_diff = 0
        time_index = 0
        
        for i in range(0,len(time)):
            time_index = int(time[i]*1000)
            if time_index > len(ref_time)-1:
                time_index = len(ref_time)-1
            omega_diff = speed[i] - ref_speed[time_index]
            angle_diff = position[i] - ref_position[time_index]
            
            J_list.append((omega_diff**2)+(angle_diff**2))
            
        J = sum(J_list)
        
        J /= len(time)
        print('The performance metric, J, is : ' + str(int(J)))
        pass
    else:
        pass