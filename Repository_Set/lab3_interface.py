# -*- coding: utf-8 -*-
"""
@file lab3_interface.py

@brief      A task and that tracks serial input from the user and sends
            relevant information to the encoder task.

@details    This task uses the UART class to track any user input into the 
            serial window. It runs a finite-state machine between 

@n          This file also contains the encoder driver class. This class 
            creates the tools that allow the task to constantly update the
            position, obtain the position, obtain the position delta, and zero
            the position of the encoder.
            
@author     Kyle Ladtkow

@date       October 20, 2020
"""

import lab3_comms
from pyb import UART
import utime

class TaskUI:
    ## State 0: Initialization State
    S0_INIT             = 0
    
    ## State 1: Waiting for Character State
    S1_WAIT_FOR_CHAR    = 1
    
    ## State 2: Waiting for Response State
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, taskNum, user_interval, dbg=True):
        '''
        @brief Creates an interface task object.
        
        @param taskNum A number to identify the task
        
        @param user_interval An integer number of microseconds between runs
        of the task. This interval decides how often the serial is checked.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
        
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(user_interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')
            
    def run(self):
        '''
        @brief      Runs one iteration of the task.
        
        @details    For the specified interval, this task will repeat. It runs
                    through three states. State 0 is only run on task
                    startup and immediately moves to State 1. State 1 
                    continuously checks the serial to see if the user has
                    typed anything into the REPL. If the user has typed 
                    anything, it will send the ASCII value of that key to the
                    encoder task and move to State 2. In State 2, the task 
                    waits for a response from the encoder task. From the
                    response, it will output some value.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.printDebug()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printDebug()
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    lab3_comms.cmd = self.ser.readchar()
                else:
                    pass
           
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printDebug()
                # Run State 2 Code
                if lab3_comms.resp:
                    if (lab3_comms.resp != 1):
                        print(lab3_comms.resp)
                        self.transitionTo(self.S1_WAIT_FOR_CHAR)
                        lab3_comms.resp = None
                    else:
                        self.transitionTo(self.S1_WAIT_FOR_CHAR)
                        lab3_comms.resp = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if 
                    the debug variable is set.
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)