"""
@file       lab5_comms.py

@brief      A semi-global file communication file.

@details    This file acts as a "comms" system between the blinking LED task 
            and the user interface task. It stores the command from the 
            interface and passes it to the LED task. It also stores the 
            response from the LED task and passes it to the interface.

@author     Kyle Ladtkow

@date       November 20, 2020
"""

## The command character sent from the user interface task to the LED task
cmd     = None

## The response from the LED task after encoding
resp    = None
