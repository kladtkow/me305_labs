'''
@file       lab5_main.py

@brief      Main program that runs the blinking LED program and the user 
            interface in a "round-robin" fashion

@details    This is the main file run on the Nucleo. This program cycles 
            between two tasks: one that receives data from the phone app and
            one that blinks an LED at a prescribed frequency.

@author     Kyle Ladtkow

@date       November 10, 2020
'''
 
from lab5_LED import TaskBlinker, LEDDriver
from lab5_interface import TaskUI

## Interval variable, the time between checks 
interval = 0.01*(10^6)

## Encoder object, keeps track of position and delta
mcuLED = LEDDriver()

## User interface task, works with serial port
task0 = TaskUI(0, interval, dbg=False)

## Encoder task, tracks and alters the position of the motor encoder
task1 = TaskBlinker(1, interval, mcuLED, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
