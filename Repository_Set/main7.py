"""
@file       main.py

@brief      The mainscript that cycles between the motor controller and the
            Nucleo user interface tasks.
            
@details    This script constantly runs on the Nucleo, cycling between the
            data generation and the data user interface tasks in a round-robin
            fashion. This script creates these task objects and defines certain
            objects like the data sampling rate, the serial port for the Nucleo,
            the maximum sampling timespan, etc.
            
@author     Kyle Ladtkow

@date       December 4, 2020
"""
from lab7_controller import EncoderDriver, MotorDriver, ClosedLoop, TaskMotorController
from lab7_NucleoUI import TaskNucleoUI
import pyb
from pyb import UART
import utime

## Encoder interval, the time in seconds between encoder updates that will account for all movements.
encoder_interval = (32768/400000)

## Sample Interval, the time in seconds between data collection for the data generation task.
sample_interval = 1

## User Interval variable, the time in seconds between checks. 
user_interval = 0.01

## UART Serial Object, the serial object that the data user interface task will use to communicate with the PC.
myuart = UART(2)

## The inital proportional gain value
k_init = 0.05

## Initialize the reference profile lists
ref_time = []
ref_speed = []
ref_position = []

ref = open('reference.csv');

i = int(0)
j = i
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    elif (i == j):
        (t,v,x) = line.strip().split(',');
        ref_time.append(float(t))
        ref_speed.append(float(v)*30)
        ref_position.append(float(x)*30)
        j += 200
    
    elif (i == 4001 or i == 7001 or i == 8001 or i == 11001):
        (t,v,x) = line.strip().split(',');
        ref_time.append(float(t))
        ref_speed.append(float(v)*30)
        ref_position.append(float(x)*30)
    i += 1

# Can't forget to close the serial port
ref.close()

## To create the encoder driver object:
# The encoder timer object
tim_enc2 = pyb.Timer(8)
tim_enc2.init(prescaler=0,period=0xFFFF)

# The A and B encoder pin objects
pinC6_ch = tim_enc2.channel(1, pin=pyb.Pin.cpu.C6, mode=pyb.Timer.ENC_AB)
pinC7_ch = tim_enc2.channel(2, pin=pyb.Pin.cpu.C7, mode=pyb.Timer.ENC_AB)

# The encoder object with desired units for angle and speed
encoder2 = EncoderDriver(tim_enc2, pinC6_ch, pinC7_ch, 'deg', 'rpm')

## To create the motor driver object:
# The NSLEEP pin object
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)

# The IN1 and IN2 pin objects for the motor
IN1_pin = pyb.Pin(pyb.Pin.cpu.B4)
IN2_pin = pyb.Pin(pyb.Pin.cpu.B5)

# The timer object for the motor
tim_mot1 = pyb.Timer(3, freq=20000)

# The IN3 and IN4 channel objects
IN1_ch = tim_mot1.channel(1, pyb.Timer.PWM, pin=IN1_pin)
IN2_ch = tim_mot1.channel(2, pyb.Timer.PWM, pin=IN2_pin)

# The motor object
motor1 = MotorDriver(nSLEEP_pin, IN1_ch, IN2_ch, tim_mot1, 1)

# Enables the motor
motor1.enable()

# The closed-loop proportional gain controller object
closedloop = ClosedLoop(k_init)

## Data user interface task, allows the Nucleo to use the serial port to communicate with the PC.
task0 = TaskNucleoUI(0, user_interval, myuart, dbg=False)

## Motor controller task, collects angular position versus time.
task1 = TaskMotorController(1, encoder_interval, ref_time, ref_speed, closedloop, encoder2, motor1, dbg=False)

## The task list contains the tasks to be run "round-robin" style.
taskList = [task0,
            task1]

## Run the tasks "round-robin" style.
while True:
    for task in taskList:
        task.run()