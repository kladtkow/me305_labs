"""
@file       lab4_DataGen.py

@brief      A task and object that tracks and reports the angular position of
            a motor encoder over time.

@details    The task in this file is intended to generate angular position
            data over a certain period of time. It does this by constantly
            updating the position of the encoder using the MCU's built-in
            clock. It reads what command is present in the lab4_comms.py file
            and, if that command is the ASCII number for 'g', it will begin 
            generating data. It will collect position data every 0.2 seconds
            for up to 10 seconds, unless the command becomes ASCII for 's' 
            beforehand. It collects data by adding it to the data array in 
            lab4_comms.py.

@n          This file also contains the encoder driver class. This class 
            creates the tools that allow the task to constantly update the
            position, obtain the position, obtain the position delta, and zero
            the position of the encoder. For the purposes of this lab, we only
            used the position-finding tool.
            
@author     Kyle Ladtkow

@date       November 3, 2020
"""

import lab4_comms
import utime
import pyb
import array

class TaskDataGen:
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    ## Collect Data State
    S2_COLLECT_DATA     = 2
    
    def __init__(self, taskNum, encoder_interval, sample_interval, sample_timespan, encoder, dbg=True):
        '''
        @brief Creates an encoder data generation task object.
        
        @param taskNum A number to identify the task
        
        @param encoder_interval An integer number of microseconds between runs
        of the task. This interval should be calculated so that now encoder 
        ticks are missed.
        
        @param sample_interval An integer number of microseconds in between
        data sampling. In this case, it is 0.2s, or 2e5 microseconds.
        
        @param sample_timespan An integer number of microseconds determining
        the maximum amount of time the data can be generated for. In this case,
        it is 10s, or 1e7 microseconds.
        
        @param encoder An encoder drive object to be used in the task.
        
        @param dbg A boolean indicating whether the task should print a debug
        message or not.
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ## Time in between data collections in microseconds
        self.sample_interval = sample_interval
        
        ## Maximum time the data collection can go on for in microseconds
        self.sample_timespan = sample_timespan
        
        ## The amount of time in microseconds between runs of the task
        self.interval = int(encoder_interval)
        
        ## The encoder object
        self.encoder = encoder
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## Initializes the variable representing when to next sample for data collection
        self.next_sample = 0
        
        self.end_sample = 0
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
                
        
        if self.dbg:
            print('Created encoder task')
        
    def run(self):
        '''
        @brief      Runs one iteration of the task

        @details    For the specified interval, this task will repeat. It runs
                    through three states, one that is only run at task startup,
                    one that constantly checks for commands from the DataUI
                    task, and one that collects data until a stop condition is
                    met.
        
        @n          For the purposes of this lab, the commands this task will
                    take from the DataUI task are the ASCII numbers for 'g'
                    and 's'. If this task receives 'g' from the lab4_comms.py
                    file, it will transition to data collection until a stop
                    condition is met. If this task receives an 's' from the
                    lab4_comms.py file or the data generation has run for the
                    specified interval, the task will stop collecting data and
                    allow the DataUI task to send it off.
                    
        @n          While this task does reuse code from my Lab 3 encoder task,
                    we will not use the commands 'p', 'z', or 'd'. If those
                    commands were to be typed into the serial port, it would
                    work as intended, it just wouldn't be brought onto the PC.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
       
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printDebug()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printDebug()
                self.encoder.update()
                
                # Run State 1 Code
        
                if lab4_comms.cmd:
                
                    if (lab4_comms.cmd==122):
                        lab4_comms.resp = self.encoder.zeroPosition()
                        lab4_comms.cmd = None
                   
                    elif(lab4_comms.cmd==112):
                        lab4_comms.resp = self.encoder.getPosition()
                        lab4_comms.cmd = None
                   
                    elif(lab4_comms.cmd==100):
                        lab4_comms.resp = self.encoder.getDelta()
                        lab4_comms.cmd = None
                   
                    elif(lab4_comms.cmd==103):
                        self.startTime = self.curr_time
                        lab4_comms.data = array.array('i',2*[0])
                        lab4_comms.cmd = None
                        self.end_sample = utime.ticks_add(self.startTime, self.sample_timespan)
                        self.next_sample = utime.ticks_add(self.startTime, self.sample_interval)
                        self.transitionTo(self.S2_COLLECT_DATA)
                  
                    else:
                        lab4_comms.resp = 1
                        lab4_comms.cmd = None
            
            elif(self.state == self.S2_COLLECT_DATA):
                self.encoder.update()
               
                if (utime.ticks_diff(self.curr_time, self.next_sample) >= 0):
                    self.encoder.collectData(self.startTime, lab4_comms.data)
                    self.next_sample = utime.ticks_add(self.curr_time, self.sample_interval)
               
                    if ((lab4_comms.cmd == 115) or (utime.ticks_diff(self.curr_time, self.end_sample) >= 0)):
                        lab4_comms.cmd = None
                        lab4_comms.resp = 2
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                   
                    else:
                        pass
               
                else:
                    pass
           
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief      Updates the state variable.
        '''
        self.state = newState

    def printDebug(self):
        '''
        @brief      Prints a debug statement with a detailed trace message if
                    the debug variable is set.
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

class EncoderDriver:
    def __init__(self):
        '''
        @brief Creates an encoder drivers object.
        '''
        self.tim = pyb.Timer(3)
        self.tim.init(prescaler=0,period=0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        self.oldposition = 0
        self.newposition = 0
        self.position = 0
        self.converter = 360/28
        self.angleposition = 0
        
        self.timestamp = 0
        self.zeroOffset = 0
        self.storedDelta = 0
    
    def update(self):
        '''
        @brief      Updates the position variable to reflect the real position.
        
        @details    This function probes the encoder to find the position. It
                    finds the position delta between the previous position
                    and the newest value. It then checks if the delta is 
                    "good" or "bad" and adjusts it if it is bad. It then adds
                    the delta to the previous position and updates that value
                    to be the current position. It also stores the delta if
                    it is not 0.
        
        @n          For this lab, it was relevant to have the encoder position
                    as an actual angle value (degrees) rather than a tick. We
                    determined that 28 ticks on the encoder's rotation
                    corresponded to 360 degrees of rotation. Therefore, we 
                    used that conversion factor to give angular data in the
                    collect data function.
        '''
        self.tick1 = self.position
        self.tick2 = self.tim.counter()-self.zeroOffset
        self.delta = self.tick2-self.tick1
       
        if(abs(self.delta)>((0xFFFF+1)/2)):
            if(self.delta>0):
                self.delta -= 0xFFFF+1
            elif(self.delta<0):
                self.delta += 0xFFFF+1
            else:
                print('ERROR')
        elif(abs(self.delta)<=((0xFFFF+1)/2)):
            pass
        else:
            print('ERROR')
        self.position += self.delta
        self.angleposition = int(self.position*self.converter)
        self.timestamp = utime.ticks_us()
        
        if(self.delta != 0):
            self.storedDelta = self.delta
        else:
            pass
        
    def getDelta(self):
        '''
        @brief      Returns the previous non-zero delta.
        '''
        return 'The delta is: ' + str(self.storedDelta)
    
    def getPosition(self):
        '''
        @brief      Returns the current position.
        '''
        return 'The position is: ' + str(self.position)
    
    def zeroPosition(self):
        '''
        @brief      Resets the position to 0.
        '''
        self.zeroOffset = self.tim.counter()
        self.position = 0
        return 'The position is now: ' + str(self.position)
    
    def collectData(self, startTime, array):
        '''
        @brief      Appends time and angular position data to an array.
        
        @details    This function takes a given data collection starting time
                    and, using values from the update function, collects an
                    angular position value for a given time; that time is 
                    measured relative to the given starting time.
        '''
        array.append(utime.ticks_diff(self.timestamp, startTime))
        array.append(self.angleposition)
        
    