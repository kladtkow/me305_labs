"""
@file       main.py

@brief      The mainscript that cycles between the data generation and the
            data user interface tasks.
            
@details    This script constantly runs on the Nucleo, cycling between the
            data generation and the data user interface tasks in a round-robin
            fashion. This script creates these task objects and defines certain
            objects like the data sampling rate, the serial port for the Nucleo,
            the maximum sampling timespan, etc.
            
@author     Kyle Ladtkow

@date       November 3, 2020
"""

from lab4_DataGen import TaskDataGen, EncoderDriver
from lab4_DataUI import TaskDataUI
from pyb import UART

## Encoder interval, the time in microseconds between encoder updates that will account for all movements.
encoder_interval = (32768/11200)*(10**6)

## Sample Interval, the time in microseconds between data collection for the data generation task.
sample_interval = int((1/5)*(10**6))

## Sample Timespan, the maximum time in microseconds that the data generation procedure can run for.
sample_timespan = int((30)*(10**6))

## UART Serial Object, the serial object that the data user interface task will use to communicate with the PC.
myuart = UART(2)

## User Interval variable, the time in microseconds between checks. 
user_interval = int(0.01*(10**6))

## Encoder object, keeps track of position and delta
encoder = EncoderDriver()

## Data user interface task, allows the Nucleo to use the serial port to communicate with the PC.
task0 = TaskDataUI(0, user_interval, myuart, dbg=False)

## Data generation task, collects angular position versus time.
task1 = TaskDataGen(1, encoder_interval, sample_interval, sample_timespan, encoder, dbg=False)

## The task list contains the tasks to be run "round-robin" style.
taskList = [task0,
            task1]

## Run the tasks "round-robin" style.
while True:
    for task in taskList:
        task.run()