'''
@file HW0x00_FSM_ME305.py

@brief A file containing classes and objects used to simulate a two-floor elevator.

@details    This file serves as an example implementation of a 
            finite-state-machine using Python. This example uses a 
            finite-state-machine to simulate an elevator going between two 
            floors, varying depending on which buttons are pressed, where the
            elevator currently is, the state of the motor, and if sensors on 
            each floor detect the motor.

'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite-state-machine to control the
                operation of an elevator going between two floors. It tracks
                and reports the position of the elevator, as well as how many
                runs and how much time has elapsed.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1
    
    ## Constant defining State 2
    S2_MOVING_UP        = 2
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR_1  = 3
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2  = 4

    
    def __init__(self, interval, motor, button_1, button_2, first, second, Instance):
        '''
        @brief      Creates a TaskElevator object. 
        @details    This class's initialization includes the interval in 
                    between runs, the motor object, the two floor button 
                    objects, the two 
                    floor sensor objects, and the elevator instance object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.motor = motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the Floor1Button object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the Floor2Button object
        self.button_2 = button_2
        
        ## The button object used for the Floor1Sensor
        self.first = first
        
        ## The button object used for the Floor2Sensor
        self.second = second
        
        ## The object for which elevator we are running.
        self.Instance = Instance
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the elevator task.
        @details    This command runs the task in between 5 different elevator 
                    states, 3 different motor states, and 2 floor states. When 
                    the task reaches a new elevator state, it prints the 
                    instance of elevator the task is on, what elevator state 
                    the task is on, and how many runs the task has completed.
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print('Elevator #' + str(self.Instance) + ', Run #' + str(self.runs) + ', State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.button_1.clearButton()
                self.button_2.clearButton()
                
                self.first.offFloor()
                self.second.offFloor()
                
                self.motor.Stop()
                
                self.transitionTo(self.S1_MOVING_DOWN)
            
            elif(self.state == self.S1_MOVING_DOWN):
                print('Elevator #' + str(self.Instance) + ', Run #' + str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                self.motor.Move_Down()
                
                self.button_1.clearButton()
                self.second.offFloor()
                
                self.transitionTo(self.S3_STOPPED_FLOOR_1)
                
            elif(self.state == self.S2_MOVING_UP):
                print('Elevator #' + str(self.Instance) + ', Run #' + str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.motor.Move_Up()
                
                self.button_2.clearButton()
                self.first.offFloor()
                
                self.transitionTo(self.S4_STOPPED_FLOOR_2)
                
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                print('Elevator #' + str(self.Instance) + ', Run #' + str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                self.first.onFloor()
                self.motor.Stop()
                
                buttonPress1 = self.button_1.getButtonState()
                buttonPress2 = self.button_2.getButtonState()
                
                if (buttonPress2 == 1):
                    self.transitionTo(self.S2_MOVING_UP)
                else:
                    pass
            
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                print('Elevator #' + str(self.Instance) + ', Run #' + str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                self.second.onFloor()  
                self.motor.Stop()
                
                buttonPress1 = self.button_1.getButtonState()
                buttonPress2 = self.button_2.getButtonState()
                
                if (buttonPress1 == 1):
                    self.transitionTo(self.S1_MOVING_DOWN)
                else:
                    pass
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp

    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the current elevator state 
                    to the next state.
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class.
    @details    This class represents a button that can be used to select 
                which floor the elevator goes to.
    '''
    
    def __init__(self):
        '''
        @brief      Creates a button object.
        '''
    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([1, 0])
    
    def clearButton(self):
        '''
        @brief      Clears the current button state by returning a 0.
        '''
        return 0

class elevatorInstance:
    '''
    @brief      An elevator instance class.
    @details    This class represents which elevator the task is dealing with.
    '''
    def __init__(self):
        '''
        @brief      Creates an elevator instance object
        '''
        pass

class ElevatorPosition:
    '''
    @brief      An elevator position.
    @details    This class represents the position of the elevator in relation to the two floor sensors. The floor sensor should output as True if the elevator is at that floor and False otherwise.
    '''
    def __init__(self):
        '''
        @brief      Creates an elevator position object
        '''
        pass
    def onFloor(self):
        '''
        @brief      Defines the elevator as on a certain floor.
        '''
        return 1
    def offFloor(self):
        '''
        @brief      Defines the elevator as not on a certain floor.
        '''
        return 0

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class controls the motion of the motor controlling the elevator. It contains code to move the elevator up, to move the elevator down, and to stop the elevator.
    '''
    
    def __init__(self):
        '''
        @brief      Creates a motor driver Object
        '''
        pass
    
    def Move_Up(self):
        '''
        @brief      Moves the elevator upward
        '''
        print('Elevator moving upward')
        return 1
    
    def Move_Down(self):
        '''
        @brief      Moves the elevator downward
        '''
        print('Elevator moving downward')
        return 2
    
    def Stop(self):
        '''
        @brief      Stops the motion of the elevator
        '''
        print('Elevator is stopped')
        return 0
