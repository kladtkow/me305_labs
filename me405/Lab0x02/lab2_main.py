"""
@file       lab2_main.py
@brief      Code used to run a reflex tester on a Nucleo microcontroller
@details    This file contains the functions and code necessary to run a reflex
            test on a Nucleo-L476RG Development Board. When run on the board,
            the LD2 LED (CPU Pin A5) will flash at a random interval between 
            two and three seconds. It will stay on for one second and repeat 
            the cycle unless the User Input button is pressed (board Pin PC13).
            The code will log the time elapsed between the light turning on and
            the press of the button (in microseconds) and then reset the cycle.
            The code will continue on like this until Ctrl + C is pressed, at
            which point it will calculate and display the average reaction 
            time in microseconds.
@n          This code makes use of the utime class in the MicroPython library.
            All times are based off of the utime counter rather than another
            timer on the Nucleo. This was done to avoid the problem of clock
            overflow and because it was decided that the smallest time change
            was sufficiently small enough for the purposes of this script.
@author     Kyle Ladtkow
@date       January 26, 2021
"""

import pyb
import utime
import random

class LEDDriver:
    '''
    @brief      A microcontroller LED class.
    @details    This class contains drivers to operate the LD2 LED on the 
                Nucleo-L476RG Development Board. It initializes the LED object
                and can turn it on, turn it off, or toggle the state.
    '''
    def __init__(self):
        '''
        @brief Creates an LED drivers object.
        '''
        ## The LED pin object
        self.LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## The state of the LED, whether or not it is on or off
        self.LEDState = 0
    
    def toggle(self):
        '''
        @brief Toggles the LED from on to off or off to on, depending on the current state
        '''
        ## If the LED is off, this function will turn it on
        if self.LEDState == 0:
            self.LED.high()
            self.LEDState = 1
        
        ## If the LED is on, this function will turn it off
        elif self.LEDState == 1:
            self.LED.low()
            self.LEDState = 0
        
        ## If everything fails, this will print an error message
        else:
            print('ERROR: Something has gone wrong in the toggle function.')
            
    def on(self):
        '''
        @brief Turns the LED on
        '''
        self.LED.high()
        self.LEDState = 1
        
    def off(self):
        '''
        @brief Turns the LED off
        '''
        self.LED.low()
        self.LEDState = 0

## The LED drivers object for the LD2 LED
mcuLED = LEDDriver()    #Creates the mcuLED object

## The start time variable for the blink interval
startTime = utime.ticks_us()    #Creates an initial starting time based on the current time

## The blink interval variable, a random number of microseconds between 2e6 and 3e6 microseconds
blinkInterval = random.randint(2*(10**6), 3*(10**6))    #Creates an initial blink interval

## The time variable for the next blink, summing the start time and the blink interval
nextBlink = utime.ticks_add(startTime, blinkInterval)   #Creates an initial next blink time

## The time variable for the start of an LED blink
blinkStart = 0  #Initializes the blinkStart variable

## The time variable for the end of an LED blink, if the button has not been pressed
blinkEnd = 0    #Initializes the blinkEnd variable

## The time variable representing the reaction time, the number of microseconds between the start of the blink and the button press
reaction = 0    #Initializes the reaction variable

## The list of reaction times for every time the LED blinks
reactionTimes = []  #Initializes the reactionTimes list

## The sum of reaction times from every time the LED blinked
reactionTimeSum = 0 #Initializes the reactionTimeSum variable

## The average reaction time from every time the LED blinked
reactionTimeAve = 0 #Initializes the reactionTimeAve variable

def count_isr(pin):
    '''
    @brief      A function that performs an interrupt service routine (ISR)
    @details    When this function is called, it calls the global variable
                for the reaction time and assigns a new reaction time to it.
    '''
    global reaction     #Calls the global variable for reaction time
    reaction = utime.ticks_diff(currTime, blinkStart)   #Assigns the newest reaction time to the variable

## The pyb object representing the external interrupt protocol
extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, count_isr)  #Creates and defines the external interrupt protocols, including what edge to trigger on, what pin to observe, and what ISR to call

## The loop that runs the entire code
while True:
    ## If there is no exception, the try code will run the main portion of the script
    try:
        ## The current time, taken in microseconds from the utime clock
        currTime = utime.ticks_us()     #Gets the current time from the utime clock.
        
        ## If the current time exceeds the time for the next LED blink (and if the LED is currently OFF), the following code will run:
        if (utime.ticks_diff(currTime, nextBlink) >= 0) and (mcuLED.LEDState == 0):
            mcuLED.on()     #Turns the LED on
            blinkStart = currTime   #Defines the blink start time as the current time
            blinkEnd = utime.ticks_add(blinkStart, 10**6)   #Defines the blink end time as the current time plus one second
            
        ## If the current time exceeds the time for the end of the LED blink with no button press (and if the LED is currently ON), the following code will run:    
        elif (utime.ticks_diff(currTime, blinkEnd) >= 0) and (mcuLED.LEDState == 1):
            mcuLED.off()    #Turns the LED off
            reaction = 0    #Resets the reaction variable to zero
            reactionTimes.append(reaction)  #Adds the zero reaction time to the reactionTimes list 
            
            startTime = currTime    #Defines the interval start time as the current time
            blinkInterval = random.randint(2*(10**6), 3*(10**6))    #Defines the interval between blinks as a random microsecond value between 2e6 and 3e6 microseconds
            nextBlink = utime.ticks_add(startTime, blinkInterval)   #Defines the interval end time as the sum of the start time and the blink interval
        
        ## If a reaction has been recorded by the ISR that is greater than 0 seconds (and if the LED is currently ON), the following code will run:
        elif ((reaction > 0) and mcuLED.LEDState == 1):
            mcuLED.off()    #Turns the LED off
            
            reactionTimes.append(reaction)  #Adds the most recent reaction time to the reactionTimes list
            reaction = 0    #Resets the reaction time variable to zero
            
            startTime = currTime    #Defines the interval start time as the current time
            blinkInterval = random.randint(2*(10**6), 3*(10**6))    #Defines the interval between blinks as a random microsecond value between 2e6 and 3e6 microseconds
            nextBlink = utime.ticks_add(startTime, blinkInterval)   #Defines the interval end time as the sum of the start time and the blink interval
            
    ## If there is a KeyboardInterrupt error detected, the code will break from the while loop and progress to the remainder of the code
    except KeyboardInterrupt:
        break

## The following code sums all of the recorded reaction times
for i in range(0,len(reactionTimes)):       # For every reaction time in the reactionTime list
    reactionTimeSum += reactionTimes[i]     # Adds the current reaction time to the reactionTimeSum 

reactionTimeAve = int(reactionTimeSum/len(reactionTimes))   #Calculates the average reaction time

## If the reaction time is zero, the following code will run, displaying an error message:
if (reactionTimeAve == 0):
    print('ERROR: No reaction time was successfully measured.')     #Prints an error message denoting that no reaction was ever recorded
else:
    print('The average reaction time was: ' + str(reactionTimeAve) + ' microseconds.')      #Prints a message with the average reaction time in microseconds