def getChange(price, payment):
    ## Calculate the actual dollar amount (in total cents) paid based on the tuple
    total_paid = int((payment[0]) + (payment[1]*5) + (payment[2]*10) + (payment[3]*25) + (payment[4]*100) + (payment[5]*500) + (payment[6]*1000) + (payment[7]*2000))
    price = int(price*100)
    ## Calculate the difference between the payment and the price
    difference = total_paid - price
    
    ## If the payment is greater than the price, proceed as normal. If not, give an error and give the payment back.
    if (difference >= 0):
        ## Give back the change
        
        ## Calculate how many twenties will fit into the change.
        if(difference/2000 >= 1):
            num_twenties = int((difference - (difference%2000))/2000)
            difference = int(difference - (num_twenties*2000))
        elif(difference/2000 < 1):
            num_twenties = 0
        else:
            print('ERROR: Something has gone in the TWENTIES calculation!')
        
        ## Calculate how many tens will fit into the change
        if(difference/1000 >= 1):
            num_tens = int((difference - (difference%1000))/1000)
            difference = int(difference - (num_tens*1000))
        elif(difference/1000 < 1):
            num_tens = 0
        else:
            print('ERROR: Something has gone in the TENS calculation!')
        
        ## Calculate how many fives will fit into the change
        if(difference/500 >= 1):
            num_fives = int((difference - (difference%500))/500)
            difference = int(difference - (num_fives*500))
        elif(difference/500 < 1):
            num_fives = 0
        else:
            print('ERROR: Something has gone in the FIVES calculation!')
        
        ## Calculate how many ones will fit into the change
        if(difference/100 >= 1):
            num_ones = int((difference - (difference%100))/100)
            difference = int(difference - (num_ones*100))
        elif(difference/100 < 1):
            num_ones = 0
        else:
            print('ERROR: Something has gone in the ONES calculation!')
        
        ## Calculate how many quarters will fit into the change
        if(difference/25 >= 1):
            num_quarters = int((difference - (difference%25))/25)
            difference = int(difference - (num_quarters*25))
        elif(difference/25 < 1):
            num_quarters = 0
        else:
            print('ERROR: Something has gone in the QUARTERS calculation!')
    
        ## Calculate how many dimes will fit into the change
        if(difference/10 >= 1):
            num_dimes = int((difference - (difference%10))/10)
            difference = int(difference - (num_dimes*10))
        elif(difference/10 < 1):
            num_dimes = 0
        else:
            print('ERROR: Something has gone in the DIMES calculation!')
        
        ## Calculate how many nickels will fit into the change
        if(difference/5 >= 1):
            num_nickels = int((difference - (difference%5))/5)
            difference = int(difference - (num_nickels*5))
        elif(difference/5 < 1):
            num_nickels = 0
        else:
            print('ERROR: Something has gone in the NICKELS calculation!')
        
        ## Calculate how many pennies will fit into the change
        if(difference >= 1):
            num_pennies = int(difference - (difference%1))
            difference = int(difference - num_pennies)
        elif(difference < 1):
            num_pennies = 0
        else:
            print('ERROR: Something has gone in the PENNIES calculation!')
        
        ## Compile the calculated number of kinds of currency and put them in a tuple
        difference_tuple = (num_pennies, num_nickels, num_dimes, num_quarters, num_ones, num_fives, num_tens, num_twenties)
        ## Return the change tuple
        return difference_tuple
    
    elif (difference < 0):
        print('INSUFFICIENT FUNDS! RETURNING ORIGINAL PAYMENT')
        ## Give the payment back
        return payment

if __name__ == '__main__':
    price = 20
    payment = (9, 0, 0, 2, 1, 0, 0, 1) ## A tuple representing 3 pennies, 0 nickels, 0 dimes, 2 quarters, 1 one-dollar bill, 0 five-dollar bills, 0 ten-dollar bills, and 1 twenty-dollar bill

    change = getChange(price, payment)    
    print(change)