"""
@file main.py

@brief The mainscript which ultimately controls the tilting platform

@details This file contains the main script for this project. It imports 
the touch panel, encoder, and motor drivers we created in Labs 0x07 and 0x08.
It also imports task objects from Lab_0x09_Controller.py, which bring all of
these drivers together in a usable format.

@n This script continuously cycles between two main tasks on a defined interval.
One task (stateController) returns the state-space matrices for both the x-
and y-axes of the platform. The other task takes those matrices and initiates
the motors based off of a defined gain matrix.

@author Kyle Ladtkow
@author Daniel Coleman

@date March 17, 2021

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
"""

from Lab_0x09_EncoderDriver import EncoderDriver
from Lab_0x09_MotorDriver import MotorDriver
from Lab_0x09_TouchDriver import TouchDriver
from Lab_0x09_Controller import TaskMotor, TaskState

import pyb
import utime

## INITIALIZE CONTROL MATRIX

## The gain to be applied to x-values
K1 = 228  #Defines the K1 value
## The gain to be applied to thetay-values
K2 = 139  #Defines the K2 value
## The gain to be applied to xdot-values
K3 = 30.7  #Defines the K3 value
## The gain to be applied to thetaydot-values
K4 = 13.9   #Defines the K4 value

## INITIALIZE THE MOTORS AND THEIR PINS

## Pin object for the sleep pin on the motor controller
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);      #Initializes the nSLEEP pin object
## Pin object for the fault pin on the motor controller
pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pyb.Pin.PULL_UP);      #Initializes the nFAULT pin object

## Pin object for motor 1 pin 1 (OUT1/M1-)
pin_IN1 = pyb.Pin.cpu.B4;       #Initializes the OUT1/M1- pin object
## Pin object for motor 1 pin 2 (OUT2/M1+)
pin_IN2 = pyb.Pin.cpu.B5;       #Initializes the OUT2/M1+ pin object 
## Pin object for motor 2 pin 1 (OUT3/M2-)
pin_IN3 = pyb.Pin.cpu.B0;       #Initializes the OUT3/M2- pin object
## Pin object for motor 2 pin 2 (OUT4/M2+)
pin_IN4 = pyb.Pin.cpu.B1;       #Initializes the OUT4/M2+ pin object

## The motor timer object
moetim = pyb.Timer(3,freq=20000)        #Initializes the motor timer object

## Motor object for motor 1. This one is designated as the controller for fault and sleep pins.
moe1 = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN1, pin_IN2, moetim, 1, 2,Fault_Motor = True)       #Creates the motor 1 object
# Ensure motor 1 is not spinning and disabled
moe1.disable()      #Disable motor 1, essentially disabling both motors
moe1.set_duty(0)    #Set the motor duty to 0%

        
## Motor object for motor 2.
moe2 = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN3, pin_IN4, moetim, 3, 4)      #Creates the motor 2 object
# Ensure motor 2 is not spinning
moe2.set_duty(0)    #Set the motor duty to 0%


## INITIALIZE THE ENCODERS AND THEIR PINS

## Pin object for encoder 1, pin 1
pin_OUT1 = pyb.Pin.cpu.C6;      #Initializes the encoder 1, pin 1 pin object
## Pin object for encoder 1, pin 2
pin_OUT2 = pyb.Pin.cpu.C7;      #Initializes the encoder 1, pin 2 pin object
## Timer object for encoder 1
enctim1 = pyb.Timer(8)      #Initializes the timer object for encoder 1
## Encoder object for encoder 1
enc1 = EncoderDriver(pin_OUT1, pin_OUT2, enctim1, 1, 2)     #Creates the encoder 1 object

## Pin object for encoder 2, pin 1
pin_OUT3 = pyb.Pin.cpu.B6;      #Initializes the encoder 2, pin 1 pin object
## Pin object for encoder 2, pin 2
pin_OUT4 = pyb.Pin.cpu.B7;      #Initializes the encoder 2, pin 2 pin object
## Timer object for encoder 2
enctim2 = pyb.Timer(4)      #Initializes the timer object for encoder 2
## Encoder object for encoder 2
enc2 = EncoderDriver(pin_OUT3, pin_OUT4, enctim2, 1, 2)     #Creates the encoder 2 object

# Set the positions for both encoders to 0
enc1.set_Position(0)        #Zeroes the encoder 1 position
enc2.set_Position(0)        #Zeroes the encoder 2 position


## INITIALIZE THE TOUCH DRIVERS

## Pin object for the x-minus pin on the touch screen
pin_xm = pyb.Pin(pyb.Pin.cpu.A7)      #Initializes the x-minus pin object
## Pin object for the x-plus pin on the touch screen
pin_xp = pyb.Pin(pyb.Pin.cpu.A6)      #Initializes the x-plus pin object
## Pin object for the y-minus pin on the touch screen
pin_ym = pyb.Pin(pyb.Pin.cpu.A1)      #Initializes the y-minus pin object
## Pin object for the y-plus pin on the touch screen
pin_yp = pyb.Pin(pyb.Pin.cpu.A0)      #Initializes the y-plus pin object
## The length variable for the touch screen
length = 7.5/39.37        #Defines the length variable in meters
## The width variable for the touch screen
width = 4.5/39.37         #Defines the width variable in meters

## Touch screen object
touchScreen = TouchDriver(pin_xp, pin_xm, pin_yp, pin_ym, length, width)        #Creates the touchScreen object


## INITIALIZE THE TASK OBJECTS

## The state-space controller object
stateController = TaskState(enc1, enc2, touchScreen)        #Creates the stateController object
## The motor controller object
motorController = TaskMotor(moe1, moe2, K1, K2, K3, K4)     #Creates the motorController object

## The initial start time for the controller
startTime = utime.ticks_us()        #Defines the startTime as the current time
## The interval between controller changes
interval = 500      #Defines the interval variable in microseconds
## The next time that the controller will activate
nextTime = utime.ticks_add(startTime, interval)     #Defines the nextTime variable as one interval from the startTime
## The current time in microseconds
currentTime = 0     #Initializes the currentTime variable as 0
## The list containing state-space variables for x- and y-axes
states = []         #Initializes the states list as empty

while True:
    currentTime = utime.ticks_us()      #Sets the currentTime variable as the current time
    ## If the current time exceeds the next time, apply the controller
    if utime.ticks_diff(currentTime, nextTime) >= 0:
        states = stateController.getState(currentTime)      #Get the state-space matrices from the stateController object
        #print(str(motorController.duty1) + '%, ' + str(motorController.duty2) + '%')
        print(states)
        motorController.changeTorque(states)        #Change the motors' behaviors based on the previous state-space matrix
        nextTime = utime.ticks_add(currentTime, interval)       #Sets the next time the controller will run as one interval from the current time
    else:
        pass