'''
@file TermProject.py

@brief The main script which operates the tilting platform controller

@details This file is the mainscript for the tilting platform controller. It
         creates objects for the motors, encoders, and the touch panel. It then
         constantly reads the states of the platform, i.e. the angles of the platform,
         displacements of the ball, and their respective corresponding velocities.
         It then applies gain values to each state-space variable, generating
         torque values to balance the platform. The script then takes these
         torques and finds their corresponding motor duties, which it then applies.
         
@n      This loop of reading the state-space and applying torque continues on
        in perpetuity until a KeyboardInterrupt is triggered or the motor fault
        is triggered. The gain values were determined experimentally over many
        tests.
        
@author Daniel Coleman
@author Kyle Ladtkow

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
import micropython
import math
from pyb import Pin
from pyb import ADC

from Lab0x09MotorDriver import MotorDriver
from Lab0x09EncoderDriver import EncoderDriver
from Lab0x09TouchScreen import TouchScreen


if __name__ == "__main__":
    ## The following block of code, which contains the main controller, is tried
    try:
        # Create emergency buffer to store errors that are thrown inside ISR
        micropython.alloc_emergency_exception_buf(200)
        
        # Start Initialization
        
        ## Touch Sceen object to return the ball's position
        TS = TouchScreen (Pin.cpu.A1, Pin.cpu.A0, Pin.cpu.A7, Pin.cpu.A6, 184, 102, 92, 51 )
        
        ## Pin object for the sleep pin on the motor controller
        pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
        ## Pin object for the fault pin on the motor controller
        pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pyb.Pin.PULL_UP);
        ## Timer object for both motors
        moetim = pyb.Timer(3,freq=20000)
                
        ## Pin object for motor 1 pin 1
        pin_IN1 = pyb.Pin.cpu.B4;
        ## Pin object for motor 1 pin 2
        pin_IN2 = pyb.Pin.cpu.B5;
                
                
        ## Pin object for motor 2 pin 1
        pin_IN3 = pyb.Pin.cpu.B0;
        ## Pin object for motor 2 pin 2
        pin_IN4 = pyb.Pin.cpu.B1;
                
        ## Motor object for motor 1. This one is designated as the controller for fault and sleep pins.
        moY = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN1, pin_IN2, moetim, 1, 2,Fault_Motor = True)
        # Ensure motor 1 is not spinning and disabled
        moY.set_duty(0)
        moY.disable()
        
        ## Motor object for motor 2.
        moX = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN3, pin_IN4, moetim, 3, 4)
        # Ensure motor 2 is not spinning
        moX.set_duty(0)
        
        ## Pin object for encoder 1, pin 1
        pin_OUT1 = pyb.Pin.cpu.C6;
        ## Pin object for encoder 1, pin 2
        pin_OUT2 = pyb.Pin.cpu.C7;
        ## Timer object for encoder 1
        enctim1 = pyb.Timer(8)
        ## Encoder object for encoder 1
        encX = EncoderDriver(pin_OUT1, pin_OUT2, enctim1, 1, 2) 
            
        ## Pin object for encoder 2, pin 1
        pin_OUT3 = pyb.Pin.cpu.B6;
        ## Pin object for encoder 2, pin 2
        pin_OUT4 = pyb.Pin.cpu.B7;
        ## Timer object for encoder 2
        enctim2 = pyb.Timer(4)
        ## Encoder object for encoder 2
        encY = EncoderDriver(pin_OUT3, pin_OUT4, enctim2, 1, 2) 
            
        # Set the positions for both encoders to 0
        encY.set_Position(0)
        encY.set_Position(0)
        
        
        # Controller gains
        
        ## Multiplier to be applied to create the gains corresponding to displacements
        posMult = 26
        ## Multiplier to be applied to create the gains corresponding to velocities
        velMult = 0.05
        ## Controller gain which will be applied to x- and y-velocities
        kudot = -6.7005*velMult
        ## Controller gain which will be applied to x- and y-angular velocities
        kthetaudot =  -0.4677*velMult
        ## Controller gain which will be applied to x- and y-displacements
        ku =  -29.4578*posMult
        ## Controller gain which will be applied to x- and y-angles
        kthetau = -6.9360*posMult
        
        # Torque to duty cycle conversion
        ## Resistance of the motor
        R = 2.21
        ## Maximum DC voltage applied to the motor
        Vdc = 12
        ## Motor torque constant
        Kt = 0.0138
        ## Gain which converts torque to duty cycle
        kdutycycle = R/(Vdc*Kt)
        
        ## The old x-displacement reading
        oldX = TS.scanPosition()[0]/1000 
        ## The old y-displacement reading
        oldY = TS.scanPosition()[1]/1000 
        
        # Updates the x-direction encoder
        encX.update()
        ## The old x-direction angle reading
        oldthetaX = -encX.get_Position()*2*math.pi/4000 
        # Updates the y-direction encoder
        encY.update()
        ## The old y-direction angle reading
        oldthetaY = -encY.get_Position()*2*math.pi/4000  
        
        ## The average torque value for the x-direction motor
        avgTX = 8*[0]
        ## The average torque value for the y-direction motor
        avgTY = 8*[0]
        
        # Enable the y-direction motor
        moY.enable()
        
        ## The next time the x-direction will be checked
        nextTimeX = utime.ticks_us()
        ## The next time the y-direction will be checked
        nextTimeY = utime.ticks_us()
        
        # End Initialization
        
        while True:
            # Check the x- and y- directions and apply corrections
            if (TS.scanPosition()[2]>0.1):
                # Update the x-direction
                
                # Update the x-direction encoder
                encX.update()
                ## The new x-direction angle
                thetaX = -encX.get_Position()*2*math.pi/4000
                ## The new x-direction angular velocity
                thetaXdot = (thetaX - oldthetaX)/(utime.ticks_diff(utime.ticks_us(), nextTimeX)/1000000)
                ## The new x-displacement
                X = TS.scanPosition()[0]/1000
                ## The new x-velocity
                Xdot = (X-oldX)/(utime.ticks_diff(utime.ticks_us(), nextTimeX)/1000000)
                
                # Set the old displacements as the new displacements
                oldthetaX = thetaX
                oldX = X
                # Sets the next time that the x-direction will be scanned
                nextTimeX = utime.ticks_us()
                
                ## The torque necessary to correct the x-direction
                TX = ku*X + kthetau*thetaX + kudot*Xdot + kthetaudot*thetaXdot
                # Adds the new torque onto the average x-torque list
                avgTX.append(TX)
                # Removes and returns the 0th TX value from the average x-torque list
                avgTX.pop(0)
                # Sets the x-direction motor to the duty corresponding to the new torque
                moX.set_duty(-kdutycycle*sum(avgTX)/(4*len(avgTX)))
                
                # Update the y-direction
                
                # Update the y-direction angle
                encY.update()
                ## The new y-direction angle
                thetaY = -encY.get_Position()*2*math.pi/4000
                ## The new y-direction angular velocity
                thetaYdot = (thetaY - oldthetaY)/(utime.ticks_diff(utime.ticks_us(), nextTimeY)/1000000)
                ## The new y-displacement
                Y = TS.scanPosition()[1]/1000
                ## The new y-velocity
                Ydot = (Y-oldY)/(utime.ticks_diff(utime.ticks_us(), nextTimeY)/1000000)
                
                # Set the old displacements as the new displacements
                oldthetaY = thetaY
                oldY = Y
                
                # Sets the next time that the y-direction will be scanned
                nextTimeY = utime.ticks_us()
                
                ## The torque necessary to correct the y-direction
                TY = ku*Y + kthetau*thetaY + kudot*Ydot + kthetaudot*thetaYdot
                # Adds the new torque onto the average y-torque list 
                avgTY.append(TY)
                # Removes and returns the 0th TY value from the average y-torque list
                avgTY.pop(0)
                # Sets the y-direction motor to the duty corresponding to the new torque
                moY.set_duty(-kdutycycle*sum(avgTY)/(4*len(avgTY)))
                
            if moY.return_Fault():
                # Require user input befor preceding
                val = input("Press Enter to enable the motor: ")
                print("Motor Reset")
                # Enable the motors and reset the fault state
                moY.reset()
            

                
    except KeyboardInterrupt:
        # Disable the motors
        moY.disable()
        