# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 16:39:53 2021

@author: danie
"""
import utime
import pyb
import collections
import micropython
from pyb import Pin
from pyb import ADC
from collections import deque 

from Lab0x08MotorDriver import MotorDriver
from Lab0x08EncoderDriver import EncoderDriver
from Lab0x07TouchScreen import TouchScreen

def Init_TouchScreen():
     ## Touch Sceen object to return the ball's position
     TS = TouchScreen (Pin.cpu.A1, Pin.cpu.A0, Pin.cpu.A7, Pin.cpu.A6, 184, 102, 92, 51 )

def Init_Motors():
    ## Pin object for the sleep pin on the motor controller
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
    ## Pin object for the fault pin on the motor controller
    pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pyb.Pin.PULL_UP);
    ## Timer object for both motors
    moetim = pyb.Timer(3,freq=20000)
            
    ## Pin object for motor 1 pin 1
    pin_IN1 = pyb.Pin.cpu.B4;
    ## Pin object for motor 1 pin 2
    pin_IN2 = pyb.Pin.cpu.B5;
            
            
    ## Pin object for motor 2 pin 1
    pin_IN3 = pyb.Pin.cpu.B0;
    ## Pin object for motor 2 pin 2
    pin_IN4 = pyb.Pin.cpu.B1;
            
    ## Motor object for motor 1. This one is designated as the controller for fault and sleep pins.
    moY = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN1, pin_IN2, moetim, 1, 2,Fault_Motor = True)
    # Ensure motor 1 is not spinning and disabled
    moY.set_duty(0)
    moY.disable()
            
    ## Motor object for motor 2.
    moX = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN3, pin_IN4, moetim, 3, 4)
    # Ensure motor 2 is not spinning
    moX.set_duty(0)

def Init_Encoders():
    ## Pin object for encoder 1, pin 1
    pin_OUT1 = pyb.Pin.cpu.C6;
    ## Pin object for encoder 1, pin 2
    pin_OUT2 = pyb.Pin.cpu.C7;
    ## Timer object for encoder 1
    enctim1 = pyb.Timer(8)
    ## Encoder object for encoder 1
    encX = EncoderDriver(pin_OUT1, pin_OUT2, enctim1, 1, 2) 
        
    ## Pin object for encoder 2, pin 1
    pin_OUT3 = pyb.Pin.cpu.B6;
    ## Pin object for encoder 2, pin 2
    pin_OUT4 = pyb.Pin.cpu.B7;
    ## Timer object for encoder 2
    enctim2 = pyb.Timer(4)
    ## Encoder object for encoder 2
    encY = EncoderDriver(pin_OUT3, pin_OUT4, enctim2, 1, 2) 
        
    # Set the positions for both encoders to 0
    encY.set_Position(0)
    encY.set_Position(0)

        numStored = 5
        oldX = numStored*[0]
        oldY = numStored*[0]
        oldthetaX = numStored*[0]
        oldthetaY = numStored*[0]
        tStepSec = .05
        tStepCount = tStepSec*100000
        
        
        moY.enable()
        nextTime = utime.ticks_us()

        # End Initialization
        while True:
            #print(abs(utime.ticks_diff(utime.ticks_us(), nextTime)) >= 0)
            #print(TS.scanPosition()[2] > 0.1)
            #print(utime.ticks_us(), nextTime)
            if ((abs(utime.ticks_diff(utime.ticks_us(), nextTime)) >= 0) & TS.scanPosition()[2]>0.1):
                #nextTime = utime.ticks_us()+tStepCount
                
                encX.update()
                thetaX = -encX.get_Position()*2*math.pi/4000
                oldthetaX.append(thetaX)
                oldthetaX.pop(0)
                thetaXdot = (oldthetaX(numStored-1) - oldthetaX(0))/(numStored*tStepSec)
                X = TS.scanPosition()[0]/1000
                oldX.append(X)
                oldX.pop(0)
                Xdot = (X-oldX)/utime.ticks_diff(utime.ticks_us(), nextTimeX)/100000
                nextTimeX = utime.ticks_us()
                TX = ku*X + kthetau*thetaX + kudot*Xdot + kthetaudot*thetaXdot
                moX.set_duty(-kdutycycle*TX)
                
                encY.update()
                thetaY = -encY.get_Position()*2*math.pi/4000
                oldthetaY.append(thetaY)
                oldthetaY.pop(0)
                thetaYdot = (thetaY - oldthetaY)/utime.ticks_diff(utime.ticks_us(), nextTimeY)/100000
                Y = TS.scanPosition()[1]/1000
                oldY.append(Y)
                oldY.pop(0)
                Ydot = (Y-oldY)/utime.ticks_diff(utime.ticks_us(), nextTimeY)/100000
                
                TY = ku*Y + kthetau*thetaY + kudot*Ydot + kthetaudot*thetaYdot
                moY.set_duty(-kdutycycle*TY)
                
                #print(TX,TY)