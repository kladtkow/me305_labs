'''
@file Lab_0x08_MotorDriver.py

@brief File containing a motor driver

@author Daniel Coleman
@author Kyle Ladtkow

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
import micropython

class MotorDriver:
    ''' 
    @brief This class implements a motor driver for the ME405 board. 
    @details This class contains several functions pertaining to the motors and controller 
                on the ME 405 project board. There are functions for initializing all
                the relevant pins for the motor, enabling and disabling the motor,
                setting the duty cycle, and detecting a fault state using an interrupt.
    '''
    
    def __init__ (self, nFAULT_pin, nSLEEP_pin, IN1_pin, IN2_pin, timer, t_1, t_2, Fault_Motor = False):
         ''' 
         @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
         @param nFAULT_pin A Pin object to monitor the fault state of the motor.
         @param nSLEEP_pin A Pin object to enable and disable the motor.
         @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
         @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
         @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
         @param t_1 An integer for timer channel 1.
         @param t_2 An integer for timer channel 2.
         @param Fault_Motor A boolean to identify the motor in charge of monitoring the fault state of the motor controller.
         '''
         ## Create the motor fault pin
         self.pin_nFAULT = nFAULT_pin
         
         ## Create the motor sleep pin
         self.pin_nSLEEP = nSLEEP_pin
         
         ## Set the pin low initially to disable the motor
         self.pin_nSLEEP.low()
         
         ## Define pin object 1
         self.pin_IN1 = pyb.Pin(IN1_pin) 
         
         ## Define pin object 2
         self.pin_IN2 = pyb.Pin(IN2_pin) 
         
         ## Create a timer object for pin 1
         self.tch1 = timer.channel(t_1, pyb.Timer.PWM,pin = self.pin_IN1)
         
         ## Create a timer object for pin 2
         self.tch2 = timer.channel(t_2, pyb.Timer.PWM,pin = self.pin_IN2)
         
         if Fault_Motor:
             ## External interrupt to trigger on the fault pin falling edge. When triggered, it will call the "callback" function
             self.extint = pyb.ExtInt (self.pin_nFAULT, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.callback)
         
         ## Boolean to store the fault detected state
         self.Fault_Detected = False
         
    
    def enable (self):
         ''' 
         @brief Sets the sleep pin high. Temporarily disables/enable the interrupt
         to prevent it from triggering
         '''
         self.pin_nSLEEP.high()
         self.extint.disable()
         self.extint.enable()
    
    def disable (self):
         ''' 
         @brief Sets the sleep pin low
         '''
         self.pin_nSLEEP.low()
    
    def set_duty (self, duty):
         ''' 
         @brief This method sets the duty cycle to be sent
         to the motor to the given level. Positive values
         cause effort in one direction, negative values
         in the opposite direction.
         @param duty A signed integer holding the duty
         cycle of the PWM signal sent to the motor 
         '''
         
         # If the duty cycle is positive set t imer channel 2, else set timer
         # channel 1
         if duty > 0:
             # Prevent a duty cycle greater than 100 from being set
             if duty > 100 :
                 duty = 100
             self.tch1.pulse_width_percent(0)
             self.tch2.pulse_width_percent(duty)
         else:
             # Prevent a duty cycle greater than 100 from being set
             if duty < -100:
                 duty = -100
             self.tch2.pulse_width_percent(0)
             self.tch1.pulse_width_percent(abs(duty))
             
    def callback (self, pin):
        ''' 
         @brief This method seves as the callback function for the external interrupt.
                 It disables the motor and sets the fault state to true.
        '''
        self.disable()
        self.Fault_Detected = True
        print("Fault Detected")
        
    def reset (self):
        ''' 
         @brief This method resets the motor following a fault state and user input.
        '''
        self.enable()
        self.Fault_Detected = False
        
    def return_Fault (self):
        ''' 
         @brief This method returns the fault detected state
        '''
        return self.Fault_Detected


if __name__ == "__main__":
    # Create emergency buffer to store errors that are thrown inside ISR
    micropython.alloc_emergency_exception_buf(200)
    
    try:
        ## Pin object for the sleep pin on the motor controller
        pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
        ## Pin object for the fault pin on the motor controller
        pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pyb.Pin.PULL_UP);
        ## Timer object for both motors
        moetim = pyb.Timer(3,freq=20000)
        
        ## Pin object for motor 1 pin 1
        pin_IN1 = pyb.Pin.cpu.B4;
        ## Pin object for motor 1 pin 2
        pin_IN2 = pyb.Pin.cpu.B5;
        
        
        ## Pin object for motor 2 pin 1
        pin_IN3 = pyb.Pin.cpu.B0;
        ## Pin object for motor 2 pin 2
        pin_IN4 = pyb.Pin.cpu.B1;
        
        ## Motor object for motor 1. This one is designated as the controller for fault and sleep pins.
        moe1 = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN1, pin_IN2, moetim, 1, 2,Fault_Motor = True)
        # Ensure motor 1 is not spinning and disabled
        moe1.set_duty(0)
        moe1.disable()
        
        ## Motor object for motor 2.
        moe2 = MotorDriver(pin_nFAULT, pin_nSLEEP, pin_IN3, pin_IN4, moetim, 3, 4)
        # Ensure motor 2 is not spinning
        moe2.set_duty(0)
        
        # Enable the motors
        moe1.enable()
        # Spin motor 1
        moe2.set_duty(-50)
        
        # Run continuously
        while True:
            # If a fault state is detected by the motor
            if moe1.return_Fault():
                # Require user input befor preceding
                val = input("Press Enter to enable the motor: ")
                print("Motor Reset")
                # Enable the motor and reset the fault state
                moe1.reset()
    # Disable the motor if Ctrl+c is pressed
    except (KeyboardInterrupt):
        moe1.disable()