clear all;
sympref('FloatingPointOutput',true)

% Percent Overshoot
PO = 2; % [%]
% Settling Time
ts = .6; % [s]

% Damping Coeff
zeta = -log(PO/100)/sqrt(pi^2+log(PO/100)^2);
% Natural Frequency
wn = 4/(ts*zeta);


s1 = -zeta*wn+1i*wn*sqrt(1-zeta^2);
s2 = -zeta*wn-1i*wn*sqrt(1-zeta^2);

poles = [s1;s2;real(s1)*10;real(s2)*11];

sympref('FloatingPointOutput',true)
syms s K1 K2 K3 K4

P_des = (s-poles(1,1))*(s-poles(2,1))*(s-poles(3,1))*(s-poles(4,1));

P_des = expand(P_des);

P_des_coeff = coeffs(P_des,s);

eqn_to_solve = [-P_des_coeff(4) + 32.499*K1 - 703.2270*K2 + 3.8350;
    -P_des_coeff(3) + 0.1625*K1 + 32.499*K3 - 703.2270*K4 - 59.6130;
    -P_des_coeff(2) - 4927.5*K1 + 0.6826*K2 + 0.1625*K3 + 0.0224;
    -P_des_coeff(1) - 4927.5*K3 + + 0.6826*K4 - 790.9378];

sol = solve(eqn_to_solve, [K1, K2, K3, K4]);
sol1 = struct2cell(sol);
celldisp(sol1)