
"""
@file Lab_0x09_TouchDriver.py

@brief Drivers that allow the user to read from the touch panel

@details This script contains the drivers for a resistive touch panel. Specifically,
these drivers allow the user to read the x- and y- position of something when
it is touching the panel. It also checks the "z-" component, which represents
if anything is touching it at all.

@author Kyle Ladtkow
@author Daniel Coleman

@date March 17, 2021

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
"""

from pyb import Pin
from pyb import ADC
from pyb import udelay

class TouchDriver:
    '''
    @brief      A resistive touch panel drivers class.
    @details    This class contains drivers to read the x- and y- coordinates
                of an object touching the panel. It also contains the drivers
                to check if an object is in contact with the panel (the "z-" component).
                All of these are accomplished by reading the analog signal from
                pins on the touch panel using PYB's ADC reader class.
    '''
    def __init__(self, x_p, x_m, y_p, y_m, length, width):
        '''
        @brief Creates a touch panel drivers object
        @param x_p The pin object for the positive x pin
        @param x_m The pin object for the negative x pin
        @param y_p The pin object for the positive y pin
        @param y_m The pin object for the negative y pin
        @param length The length of the panel (long side)
        @param width The width of the panel (short side)
        '''
        
        ## The local positive x pin object
        self.x_p = x_p      #Initializes the x_p object
        ## The local negative x pin object
        self.x_m = x_m      #Initializes the x_m object
            
        ## The local positive y pin object
        self.y_p = y_p      #Initializes the y_p object
        ## The local negative y pin object
        self.y_m = y_m      #Initializes the y_m object
        
        ## The local length variable for the panel
        self.length = length        #Initializes the length variable
        ## The local width variable for the panel
        self.width = width          #Initializes the width variable
        
        ## The lowest x tick value panel
        self.xticklow = 198         #Defines the xticklow value
        ## The highest x tick value for the panel
        self.xtickhigh = 3787       #Defines the xtickhigh value
        ## The range for x tick values
        self.deltax = self.xtickhigh-self.xticklow      #Calculates the range of x tick values for the panel
        ## The tick-to-coordinate conversion factor for the x-direction
        self.xcoefficient = self.length/self.deltax     #Calculates the tick-to-position conversion factor for x
        ## The tick-to-coordinate bias for the x-direction
        self.xintercept = (self.length/2)-(self.xcoefficient*self.xtickhigh)        #Calculates the tick-to-position bias for x
        
        ## The lowest y tick value for the panel
        self.yticklow = 382         #Defines the yticklow value
        ## The highest y tick value for the panel
        self.ytickhigh = 3581       #Defines the ytickhigh value
        ## The range for y tick values
        self.deltay = self.ytickhigh-self.yticklow      #Calculates the range of y tick values for the panel
        ## The tick-to-coordinate conversion factor for the y-direction
        self.ycoefficient = self.width/self.deltay      #Calculates the tick-to-position conversion factor for y
        ## The tick-to-coordinate bias for the y-direction
        self.yintercept = (self.width/2)-(self.ycoefficient*self.ytickhigh)         #Calculates the tick-to-position bias for y
        
        ## The raw x tick value for the panel
        self.xtick = 0      #Initializes the xtick variable
        ## The raw y tick value for the panel
        self.ytick = 0      #Initializes the ytick variable
        ## The raw z value for the panel
        self.zread = 0      #Initializes the zread variable
        
        ## The boolean for whether or not the panel has been contacted
        self.contact = False        #Initializes the contact boolean
        
        ## The x-coordinate variable for the panel
        self.xcoord = 0     #Initializes the xcoord variable
        ## The y-coordinate variable for the panel
        self.ycoord = 0     #Initializes the ycoord variable
        ## The tuple containing x- and y-coordinates and the boolean for contact
        self.coords = None      #Initializes the coords tuple
        
    def scanX(self):
        '''
        @brief      Reads the x-coordinate on the panel
        @details    This function sets the four pins to their correct configurations
                    and then reads the ADC value. It then converts it to a position value
                    based on the length of the board.
        '''
        ## Initializes every pin to allow for reading
        self.y_p.init(mode=Pin.ANALOG)      #Sets y_p as analog to let it float
        self.x_m.init(mode=Pin.OUT_PP, value = 0)       #Sets x_m as an output and LOW
        self.x_p.init(mode=Pin.OUT_PP, value = 1)       #Sets x_p as an output and HIGH
        self.y_m.init(mode=Pin.ANALOG)      #Sets y_m as analog to read the position
        
        ## Allow the reading to settle by waiting ~3.6 microseconds
        udelay(4)       #Delay the read by 4 microseconds
        
        ## Reads the raw tick value for the x-axis
        self.xtick = ADC(self.y_m).read()   #Read the ADC reading from pin y_m
        ## Convert the raw tick value to position and return it
        return (self.xcoefficient*self.xtick) + self.xintercept     #Converts the raw tick reading to position
        
    def scanY(self):
        '''
        @brief      Reads the y-coordinate on the panel
        @details    This function sets the four pins to their correct configurations
                    and then reads the ADC value. It then converts it to a position value
                    based on the width of the board.
        '''        
        ## Initializes every pin to allow for reading
        self.x_p.init(mode=Pin.ANALOG)      #Sets x_p as analog to let it float
        self.y_m.init(mode=Pin.OUT_PP, value = 0)       #Sets y_m as an output and LOW 
        self.y_p.init(mode=Pin.OUT_PP, value = 1)       #Sets y_p as an output and HIGH
        self.x_m.init(mode=Pin.ANALOG)      #Sets x_m as analog to read the position
        
        ## Allow the reading to settle by waiting ~3.6 microseconds
        udelay(4)       #Delay the read by 4 microseconds
        
        ## Reads the raw tick value for the y-axis
        self.ytick = ADC(self.x_m).read()   #Read the ADC reading from pin x_m
        ## Convert the raw tick value to position and return it
        return (self.ycoefficient*self.ytick) + self.yintercept     #Converts the raw tick reading to position
    
    def checkZ(self):
        '''
        @brief      Checks if something is making contact with the panel
        @details    This function sets the four pins to their correct configurations
                    and then reads the ADC value. Based on the magnitude of the ADC
                    value, it determines whether or not something is making contact
                    with the panel.
        '''
        ## Initializes every pin to allow for reading
        self.y_m.init(mode=Pin.ANALOG)      #Sets y_m as analog to let it float
        self.y_p.init(mode=Pin.OUT_PP, value = 1)   #Sets y_p as an output and HIGH 
        self.x_m.init(mode=Pin.OUT_PP, value = 0)   #Sets x_m as an output and LOW
        self.x_p.init(mode=Pin.ANALOG)      #Sets x_p as analog to read the ADC value
        
        ## Allow the reading to settle by waiting ~3.6 microseconds
        udelay(4)       #Delay the read by 4 microseconds
        
        ## Reads the raw tick value for the "z-" component
        self.zread = ADC(self.x_p).read()   #Read the ADC reading from x_p
        
        ## If the ADC reading is not LOW, something is contacting the panel. If not, something isn't contacting the panel.
        if (self.zread > 100):
            ## The panel has been contacted
            self.contact = True     #Sets the contact boolean as true
        else:
            ## The panel has not been contacted
            self.contact = False    #Sets the contact boolean as false
    
    def checkCoords(self):
        '''
        @brief      Reads the position of an object if it is making contact with the panel
        @details    This function first checks if something is making contact with 
                    the panel. If something is, it will read the x- and y-position
                    of that object.
        '''
        ## Check if something is making contact with the panel
        self.checkZ()   #Call checkZ to see if there is contact
        
        ## If contact is made with the board, read the x- and y- position
        if (self.contact == True):
            ## Read the x-coordinate
            self.xcoord = self.scanX()     #Read the x-coordinate and convert it into a string
            ## Read the y-coordinate
            self.ycoord = self.scanY()     #Read the y-coordinate and convert it into a string
        elif (self.contact == False):
            ## Define the x- and y- coordinates as no contact
            self.xcoord = 0      #Defines the x-coordinate as "No Contact"
            self.ycoord = 0      #Defines the y-coordinate as "No Contact"
        else:
            print('ERROR! Something has gone wrong in the checkCoords function')
        
        ## Create a tuple with the x- and y-coordinates with the contact boolean
        self.coords = (self.xcoord, self.ycoord, self.contact)     #Defines the coords tuple
        ## Return the coordinates tuple
        return self.coords      #Returns the coord variable
    
if __name__ == '__main__':
    pin_xm = Pin(Pin.cpu.A7)
    pin_xp = Pin(Pin.cpu.A6)
    pin_ym = Pin(Pin.cpu.A1)
    pin_yp = Pin(Pin.cpu.A0)
    length = 7.5
    width = 4.5
    
    touchScreen = TouchDriver(pin_xp, pin_xm, pin_yp, pin_ym, length, width)
    
    ## TEST SCRIPT: Constantly reads the x- and y-coordinates
    while True:    
        print(touchScreen.checkCoords())