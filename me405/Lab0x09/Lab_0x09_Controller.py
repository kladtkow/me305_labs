"""
@file Lab_0x09_Controller.py

@brief The drivers for both the state-space reader and the motor-controller

@details This file contains the drivers for both the sensors (touch panel and encoders)
and the motors. Each driver incorporates the drivers we made before into 
easier-to-use versions. The sensor drivers (TaskState) read the touch panel and the
encoders to create state-space matrices for the x- and y-axes of the platform.
The motor drivers (TaskMotor) take these matrices, apply gain values, and set
the duty of each motor accordingly.

@author Kyle Ladtkow
@author Daniel Coleman

@date March 17, 2021

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
"""
from math import pi
import utime

class TaskMotor:
    '''
    @brief Controls the motor based off of data from the state-space reader
    @details This task takes the state-space matrices produced by the 
    state-space reader and applies gains to calculate the corresponding torques.
    It then applies the torque to the motors.
    '''
    def __init__ (self, motor1, motor2, K1, K2, K3, K4):
        '''
        @brief Creates a motor controller object using other motor objects and gains
        @param motor1 The motor driver object for motor 1
        @param motor2 The motor driver object for motor 2
        @param K1 The gain applied to x or y to get it to torque
        @param K2 The gain applied to thetay or thetax to get it to torque
        @param K3 The gain applied to xdot or ydot to get it to torque
        @param K4 The gain applied to thetaydot or thetaxdot to get it to torque
        '''
        
        ## The local motor object for motor 1
        self.motor1 = motor1    #Creates a local object for motor 1
        self.motor1.enable()    #Enables motor 1, effectively enabling both motors
        ## The local motor object for motor 2
        self.motor2 = motor2    #Creates a local object for motor 2
        
        ## The local gain for x or y
        self.K1 = K1    #Creates a local K1 gain
        ## The local gain for thetay or thetax
        self.K2 = K2    #Creates a local K2 gain
        ## The local gain for xdot or ydot
        self.K3 = K3    #Creates a local K3 gain
        ## The local gain for thetadoty or thetadotyx
        self.K4 = K4    #Creates a local K4 gain
        
        ## The motor's torque constant
        self.K_t = 13.8/1000        #Creates a local motor torque constant variable
        ## The resistance for the motor
        self.R = 2.21       #Creates a local motor resistance variable
        ## The voltage for the motor
        self.Vdc = 12       #Creates a local motor voltage variable
        
        ## The gain required to turn torque into percent duty
        self.K_duty = self.R/(self.Vdc*self.K_t)        #Calculates the percent duty gain
        
        ## The torque in the y-direction
        self.T_Y = 0    #Initializes the y-torque variable
        ## The torque in the x-direction
        self.T_X = 0    #Initializes the x-torque variable
        
        ## The percent duty for motor 1
        self.duty1 = 0      #Initializes the variable for motor 1 duty
        ## The percent duty for motor 2
        self.duty2 = 0      #Initializes the variable for motor 2 duty
        
        ## The state-space matrix for the y-direction
        self.x_y = []       #Initializes the list for x_y
        ## The state-space matrix for the x-direction
        self.x_x = []       #Initializes the list for x_x
        
        ## The x-value of the object
        self.x_new = 0      #Initializes the x-value variable
        ## The x-speed of the object
        self.xdot = 0       #Initializes the x-speed variable
        ## The angle of the platform in the y-direction
        self.thetay_new = 0     #Initializes the thetay variable
        ## The angular velocity of the platform in the y-direction
        self.thetadoty = 0      #Initializes the thetaydot variable
        
        ## The y-value of the object
        self.y_new = 0      #Initialies the y-value variable
        ## The y-speed of the object
        self.ydot = 0       #Initializes the y-speed variable
        ## The angle of the platform in the x-direction
        self.thetax_new = 0     #Initializes the thetax variable
        ## The angular velocity of the platform in the x-direction
        self.thetadotx = 0      #Initializes the thetaxdot variable
        
    def changeTorque(self, states):
        '''
        @brief Takes the state-space variable, calculates the torque, and sets the duty of the motors accordingly
        '''
        
        self.x_y = states[0]        #Sets the y-direction state-space matrix
        self.x_x = states[1]        #Sets the x-direction state-space matrix
        
        self.x_new = self.x_y[0]    #Sets the x-value
        self.xdot = self.x_y[2]     #Sets the x-speed value
        self.thetay_new = self.x_y[1]       #Sets the thetay value
        self.thetadoty = self.x_y[3]        #Sets the thetaydot value
        
        self.y_new = self.x_x[0]    #Sets the y-value
        self.ydot = self.x_x[2]     #Sets the y-speed value
        self.thetax_new = self.x_x[1]       #Sets the thetax value
        self.thetadotx = self.x_x[3]        #Sets the thetaxdot value
        
        self.T_Y = (self.x_new*self.K1) + (self.thetay_new*self.K2) + (self.xdot*self.K3) + (self.thetadoty*self.K4)        #Calculates the torque necessary for the y-direction 
        self.T_X = (self.y_new*self.K1) + (self.thetax_new*self.K2) + (self.ydot*self.K3) + (self.thetadotx*self.K4)        #Calculates the torque necessary for the x-direction
        
        self.duty1 = self.T_X*self.K_duty       #Calculates the duty for motor 1
        self.duty2 = self.T_Y*self.K_duty       #Calculates the duty for motor 2
        
        self.motor1.set_duty(self.duty1)
        self.motor2.set_duty(self.duty2)
        
class TaskState:
    '''
    @brief Constantly reads the state of the platform
    '''
    def __init__ (self, enc1, enc2, touchScreen):
        self.enc1 = enc1
        self.enc2 = enc2
        self.touchScreen = touchScreen
    
        
        self.t1 = 0
                
        self.thetay_old = 0
        self.thetay_new = 0
        
        self.thetadoty = 0
        
        self.thetax_old = 0
        self.thetax_new = 0
        
        self.thetadotx = 0

        self.coords = None
        self.contact = None
        
        self.x_old = 0
        self.x_new = 0
        
        self.y_old = 0
        self.y_new = 0
        
        self.xdot = 0
        self.ydot = 0
        
        self.x_y = []
        self.x_x = []
        
    def getState(self, t2):        
        ## Read the encoders to get angles and angular velocities
        self.enc1.update()
        self.enc2.update()
        self.thetay_new = ((2*pi)/4000)*self.enc2.get_Position()
        self.thetax_new = ((2*pi)/4000)*self.enc1.get_Position()
        
        self.thetadoty = (self.thetay_new-self.thetay_old)/(utime.ticks_diff(t2, self.t1)*(10**6))
        self.thetadotx = (self.thetax_new-self.thetax_old)/(utime.ticks_diff(t2, self.t1)*(10**6))
        
        self.coords = self.touchScreen.checkCoords()
        
        self.x_new = self.coords[0]
        self.y_new = self.coords[1]
        
        self.contact = self.coords[2]
        
        self.xdot = (self.x_new - self.x_old)/(utime.ticks_diff(t2, self.t1)*(10**6))
        self.ydot = (self.y_new - self.y_old)/(utime.ticks_diff(t2, self.t1)*(10**6))
        
        if self.contact == True:
            self.x_y = [self.x_new, self.thetay_new, self.xdot, self.thetadoty]
            self.x_x = [self.y_new, self.thetax_new, self.ydot, self.thetadotx]
        elif self.contact == False:
            self.x_y = [0, self.thetay_new, 0, self.thetadoty]
            self.x_x = [0, self.thetax_new, 0, self.thetadotx]
        else:
            pass
        
        self.thetay_old = self.thetay_new
        self.thetax_old = self.thetax_new
        
        self.x_old = self.x_new
        self.y_old = self.y_new
        
        self.t1 = t2
        
        return [self.x_y, self.x_x]