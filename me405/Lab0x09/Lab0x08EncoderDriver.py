'''
@file Lab_0x08_EncoderDriver.py

@brief This file contains the encoder class 

@author Daniel Coleman
@author Kyle Ladtkow

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
import math

class EncoderDriver:
    '''
    @brief      A class that contains the methods to initialize and update a motor encoder
    @details This class contains several funtions pertaining to the motor encoders on the 
                ME 405 project board. There are funtions for initializing the relavant pins 
                for the encoder, updating the encoder, returning encoder position, 
                setting encoder position and returnign encoder delta.
    '''
    
    def __init__(self, OUT1_pin, OUT2_pin, timer, t_1, t_2):
        '''
        @brief                  Initializes the encoder object
        @param OUT1_pin            A pin object for encoder channel 1
        @param OUT2_pin            A pin object for encoder channel 2
        @param timer     A timer object for the encoder  
        @param t_1    An integer corresponding to timer channel 1
        @param t_2    An integer corresponding to timer channel 1

        '''
        
        ## Define pin 1 locally
        self.Pin_1 = OUT1_pin
        
        ## Define pin 2 locally
        self.Pin_2 = OUT2_pin
        
        ## Set the period for the timer
        self.Period = 0xFFFF
        
        ## Create the timer object
        self.tim = timer
        
        ## Initialize the timer
        self.tim.init(prescaler = 0, period = self.Period)
        
        ## Create timer channel 1
        self.tim.channel(t_1, pin = self.Pin_1, mode = pyb.Timer.ENC_AB)
        
        ## Create timer channel 2
        self.tim.channel(t_2, pin = self.Pin_2, mode = pyb.Timer.ENC_AB)
        
        ## Set the encoder state to 0
        self.Current_State = 0
        
        ## Set the encoder position to 0
        self.Position = 0
        
        ## Set the encoder delta to 0
        self.Delta = 0

    def update(self):
        '''
        @brief      Updates values within the encoder class that define its position
        @details    This method updates the delta and current_state values to reflect the 
                    current state of the encoder. Then it decides if the encoder has 
                    overflowed, underflowed, or neither, and updates the absolute
                    position of the encoder since the last restart
        '''
        self.Delta = self.Current_State - self.tim.counter()
        self.Current_State = self.tim.counter()
        
        # If the encoder has underflower or overflowed, correct the delta value
        if abs(self.Delta) > self.Period/2:
            if self.Delta > 0:
                self.Delta = -(self.Period-self.Delta)

            else:
                self.Delta = (self.Period+self.Delta)

        # Add the delta to the position    
        self.Position += self.Delta
        
    
    def get_Position(self):
        '''
        @brief    Returns the current position of the encoder     
        @return   An integer representing the current position of the encoder
        '''  
        return self.Position
        
    def set_Position(self, New_Position):
        '''
        @brief                  Sets the current position to a desired value 
        @param New_Position     An integer value representing the desired position of the encoder
        '''
        self.Position = int(New_Position)
        
    def get_Delta(self):
        '''
        @brief   Returns the delta value between the current state and previous state
        @return  An integer value representing the difference between the two states
        '''    
        return self.Delta

if __name__ == "__main__":
    
    ## Pin object for encoder 1, pin 1
    pin_OUT1 = pyb.Pin.cpu.C6;
    ## Pin object for encoder 1, pin 2
    pin_OUT2 = pyb.Pin.cpu.C7;
    ## Timer object for encoder 1
    enctim1 = pyb.Timer(8)
    ## Encoder object for encoder 1
    enc1 = EncoderDriver(pin_OUT1, pin_OUT2, enctim1, 1, 2) 
    
    ## Pin object for encoder 2, pin 1
    pin_OUT3 = pyb.Pin.cpu.B6;
    ## Pin object for encoder 2, pin 2
    pin_OUT4 = pyb.Pin.cpu.B7;
    ## Timer object for encoder 2
    enctim2 = pyb.Timer(4)
    ## Encoder object for encoder 2
    enc2 = EncoderDriver(pin_OUT3, pin_OUT4, enctim2, 1, 2) 
    
    # Set the positions for both encoders to 0
    enc1.set_Position(0)
    enc2.set_Position(0)
    
    # Constantly update encoders and print positions
    while True:
        enc1.update()
        enc2.update()
        print(enc1.get_Position()*360/4000, enc2.get_Position()*360/4000)


