'''
@file lab1_main.py
@brief      Code used to simulate a rudimentary vending machine
@details    This file contains the functions and code necessary to simulate 
            Vendotron. Vendotron is run using a finite-state-machine (FSM) run
            between five states. When this file is run, the FSM displays the
            "drink" options and then waits for keypresses from the user. The 
            user can input funds (0-7 correspond with increasing currency
            denominations), select a drink (the commands are displayed), or 
            eject their current balance. If the balance is sufficient, the
            FSM will dispense the correct selection and any change.
@author Kyle Ladtkow
@date January 18, 2021
'''

import keyboard

## The variable representing the state of the FSM
state = 0

## The variable representing the current balance
balance = 0

## The variable representing the change
change = 0

## The variable representing what coins have been entered
coins_entered = None

## The variable representing what selection has been made
selection = None

## The next four variables represent the soda prices in cents
price_cuke = 100
price_popsi = 120
price_spryte = 85
price_drpupper = 110

## The variable representing whether a key has been pressed by the user
pushed_key = None

## The variable representing whether the eject button has been pressed by the user
eject_button = False

## The tuples representing the order of currency denominations
change_order_singular = ('penny', 'nickel', 'dime', 'quarter', 'one dollar bill', 'five dollar bill', 'ten dollar bill', 'twenty dollar bill')
change_order_plural = ('pennies', 'nickels', 'dimes', 'quarters', 'one dollar bills', 'five dollar bills', 'ten dollar bills', 'twenty dollar bills')

def on_keypress (thing):
    """ 
    @brief  Callback which runs when the user presses a key.
    @author John Ridgely
    @date January 11, 2021
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press (on_keypress)  ########## Set callback

def eject(ejecting_change):
    '''
    @brief      Returns a string of change 
    @details    This function takes in a tuple of change and returns a string
                denoting what change is returned to the user.
    @author     Kyle Ladtkow
    @date       January 18, 2021
    '''
    if (type(ejecting_change) == tuple) and (len(ejecting_change) == 8):
        eject_str = 'Ejecting:'
        for i in range(0,8):
            if(ejecting_change[i] != 0):
                if (ejecting_change[i] == 1):
                    eject_str += str('\n') + str(ejecting_change[i]) + ' ' + change_order_singular[i]
                elif (ejecting_change[i] >= 1):
                    eject_str += str('\n') + str(ejecting_change[i]) + ' ' + change_order_plural[i]
                else:
                    print('ERROR! Something has gone wrong in the eject function')
            else:
                pass
        if eject_str == 'Ejecting:':
            print('No change to dispense!')
        else:
            print(eject_str)
    else:
        print('ERROR! Input change is not the correct type and length')
    
def getChange(price, payment):
    '''
    @brief      Calculates how much change the user will receive
    @details    This function finds the difference between the payment and the
                price (both in cents). Next, it takes that difference and 
                calculates the least denominations of currency and places them
                into a tuple.
    @author     Kyle Ladtkow
    @date       January 18, 2021
    '''
    
    ## Calculate the difference between the payment and the price
    difference = payment - price
    
    ## If the payment is greater than the price, proceed as normal. If not, give an error and give the payment back.
    if (difference >= 0):
        ## Give back the change
        
        ## Calculate how many twenties will fit into the change.
        if(difference/2000 >= 1):
            num_twenties = int((difference - (difference%2000))/2000)
            difference = int(difference - (num_twenties*2000))
        elif(difference/2000 < 1):
            num_twenties = 0
        else:
            print('ERROR: Something has gone in the TWENTIES calculation!')
        
        ## Calculate how many tens will fit into the change
        if(difference/1000 >= 1):
            num_tens = int((difference - (difference%1000))/1000)
            difference = int(difference - (num_tens*1000))
        elif(difference/1000 < 1):
            num_tens = 0
        else:
            print('ERROR: Something has gone in the TENS calculation!')
        
        ## Calculate how many fives will fit into the change
        if(difference/500 >= 1):
            num_fives = int((difference - (difference%500))/500)
            difference = int(difference - (num_fives*500))
        elif(difference/500 < 1):
            num_fives = 0
        else:
            print('ERROR: Something has gone in the FIVES calculation!')
        
        ## Calculate how many ones will fit into the change
        if(difference/100 >= 1):
            num_ones = int((difference - (difference%100))/100)
            difference = int(difference - (num_ones*100))
        elif(difference/100 < 1):
            num_ones = 0
        else:
            print('ERROR: Something has gone in the ONES calculation!')
        
        ## Calculate how many quarters will fit into the change
        if(difference/25 >= 1):
            num_quarters = int((difference - (difference%25))/25)
            difference = int(difference - (num_quarters*25))
        elif(difference/25 < 1):
            num_quarters = 0
        else:
            print('ERROR: Something has gone in the QUARTERS calculation!')
    
        ## Calculate how many dimes will fit into the change
        if(difference/10 >= 1):
            num_dimes = int((difference - (difference%10))/10)
            difference = int(difference - (num_dimes*10))
        elif(difference/10 < 1):
            num_dimes = 0
        else:
            print('ERROR: Something has gone in the DIMES calculation!')
        
        ## Calculate how many nickels will fit into the change
        if(difference/5 >= 1):
            num_nickels = int((difference - (difference%5))/5)
            difference = int(difference - (num_nickels*5))
        elif(difference/5 < 1):
            num_nickels = 0
        else:
            print('ERROR: Something has gone in the NICKELS calculation!')
        
        ## Calculate how many pennies will fit into the change
        if(difference >= 1):
            num_pennies = int(difference - (difference%1))
            difference = int(difference - num_pennies)
        elif(difference < 1):
            num_pennies = 0
        else:
            print('ERROR: Something has gone in the PENNIES calculation!')
        
        ## Compile the calculated number of kinds of currency and put them in a tuple
        difference_tuple = (num_pennies, num_nickels, num_dimes, num_quarters, num_ones, num_fives, num_tens, num_twenties)
        ## Return the change tuple
        return difference_tuple
    
    elif (difference < 0):
        print('INSUFFICIENT FUNDS! RETURNING ORIGINAL PAYMENT')
        ## Give the payment back
        return payment

def printWelcome():
    '''
    @brief      Prints a welcome message upon startup
    @details    This function takes the price of each drink and displays them
                along with instructions on how to receive them.
    @author     Kyle Ladtkow
    @date       January 18, 2021
    '''
    print('Welcome to Vendotron! Please insert your payment and make your selection out of the options below:')
    print('Cuke         $' + str("%.2f" % (price_cuke/100)) + '   (Press "C")')
    print('Popsi        $' + str("%.2f" % (price_popsi/100)) + '   (Press "P")')
    print('Spryte       $' + str("%.2f" % (price_spryte/100)) + '   (Press "S")')
    print('Dr. Pupper   $' + str("%.2f" % (price_drpupper/100)) + '   (Press "D")')
    print('\nAt any time, press "E" to eject your payment.')

while True:
    '''
    Implement FSM using a while loop and many if statements that will run eternally until the user presses CTRL-C
    '''
    
    if state == 0:
        ## State 0: Initialization
        # In this state, the FSM prints the welcome statement and immediately
        # moves on to State 1.
        printWelcome()
        state = 1
    elif state == 1:
        ## State 1: Wait for Selection
        # In this state, the FSM waits for input from the user. Based on the
        # input, the FSM moves to another state.
        
        # If coins have been entered, the FSM moves briefly to State 2.
        if coins_entered != None:
            state = 2               
        
        # If a selection has been made, the FSM moves briefly to State 3.
        if selection != None:
            state = 3
            
        # If the eject button has been hit, the FSM moves briefly to State 4.
        if eject_button == True:
            state = 4
            
    elif state == 2:
        ## State 2: Coins Entered
        # In this state, the FSM adds the newly-entered coins into the balance.
        # When it is done, it moves back to State 1.
        
        # If a penny has been entered, the balance is increased by 1 cent.
        if coins_entered == 'penny':
            balance += 1
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1

        # If a nickel has been entered, the balance is increased by 5 cents.            
        elif coins_entered == 'nickel':
            balance += 5
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1

        # If a dime has been entered, the balance is increased by 10 cents.
        elif coins_entered == 'dime':
            balance += 10
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1

        # If a quarter has been entered, the balance is increased by 25 cents.
        elif coins_entered == 'quarter':
            balance += 25
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1
            
        # If a one dollar bill has been entered, the balance is increased by 100 cents.
        elif coins_entered == 'one dollar bill':
            balance += 100
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1

        # If a five dollar bill has been entered, the balance is increased by 500 cents.            
        elif coins_entered == 'five dollar bill':
            balance += 500
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1
            
        # If a ten dollar bill has been entered, the balance is increased by 1000 cents.
        elif coins_entered == 'ten dollar bill':
            balance += 1000
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1
            
        # If a twenty dollar bill has been entered, the balance is increased by 2000 cents.
        elif coins_entered == 'twenty dollar bill':
            balance += 2000
            print('Your balance is now: ' + str("%.2f" % (balance/100)))
            coins_entered = None
            state = 1
            
        else:
            print('ERROR: Something has gone wrong in State 2') 
            state = 1
            
    elif state == 3:
        ## State 3: Selection Made
        # In this state, the FSM takes the selection from the user and determines
        # its next steps. If the balance is sufficient, the FSM transitions to
        # State 5. If the balance is insufficient, the FSM transitions back to
        # State 1.
    
        if selection == 'Cuke':
            print('Cuke         $' + str("%.2f" % (price_cuke/100)))
            if balance >= price_cuke:
                change = getChange(price_cuke, balance)
                balance -= price_cuke
                print('Dispensing Cuke...')
                print('A Cuke has been dispensed. Thank you!')
                state = 5
            elif balance < price_cuke:
                print('INSUFFICIENT BALANCE! Please insert more funds')
                selection = None
                state = 1
            else:
                print('ERROR! Something has gone wrong in the Cuke section of State 3')
        
        elif selection == 'Popsi':
            print('Popsi        $' + str("%.2f" % (price_popsi/100)))
            if balance >= price_popsi:
                change = getChange(price_popsi, balance)
                balance -= price_popsi
                print('Dispensing Popsi...')
                print('A Popsi has been dispensed. Thank you!')
                state = 5
            elif balance < price_popsi:
                print('INSUFFICIENT BALANCE! Please insert more funds')
                selection = None
                state = 1
            else:
                print('ERROR! Something has gone wrong in the Popsi section of State 3')
        
        elif selection == 'Spryte':
            print('Spryte       $' + str("%.2f" % (price_spryte/100)))
            if balance >= price_spryte:
                change = getChange(price_spryte, balance)
                balance -= price_spryte
                print('Dispensing Spryte...')
                print('A Spryte has been dispensed. Thank you!')
                state = 5
            elif balance < price_spryte:
                print('INSUFFICIENT BALANCE! Please insert more funds')
                selection = None
                state = 1
            else:
                print('ERROR! Something has gone wrong in the Spryte section of State 3')
        
        elif selection == 'Dr. Pupper':
            print('Dr. Pupper   $' + str("%.2f" % (price_drpupper/100)))
            if balance >= price_drpupper:
                change = getChange(price_drpupper, balance)
                balance-= price_drpupper
                print('Dispensing Dr. Pupper...')
                print('A Dr. Pupper has been dispensed. Thank you!')
                state = 5
            elif balance < price_drpupper:
                print('INSUFFICIENT BALANCE! Please insert more funds')
                selection = None
                state = 1
            else:
                print('ERROR! Something has gone wrong in the Dr. Pupper section of State 3')
        
        else:
            print('ERROR: Something has gone wrong in State 3')
    
    elif state == 4:
        ## State 4: Eject Coins
        # In this state, the FSM ejects the current balance in the least
        # denominations of currency. It then clears the balance and returns
        # to State 1.
        
        ## If the eject button has been pressed, all of the balance is ejected.
        if eject_button == True:
            change = getChange(0, balance)
            eject(change)
            eject_button = False
            
            balance = 0
            change = 0
            
            state = 1
        
        ## If there is leftover balance from the previous transaction, that
        # change is ejected.
        elif eject_button == False:
            eject(change)
            
            balance = 0
            change = 0
            
            state = 1
        else:
            print('ERROR! Something has gone wrong in State 4')
    
    elif state == 5:
        ## State 5: Dispense Soda
        # In this state, the FSM dispenses the selected soda. If there is
        # leftover balance, it transitions to State 4. Otherwise, it transitions
        # back to State 1.
        if balance != 0:
            selection = None
            state = 4
        elif balance == 0:
            selection = None
            state = 1
        else:
            print('ERROR! Something has gone wrong in State 5')
    else:
        print('ERROR: THE FSM IS IN AN UNDECLARED STATE')
        
    try:
    # If a key has been pressed, check if it's a key we care about
        if pushed_key:
            if pushed_key == "0":
                # Add a penny to the total number of pennies
                coins_entered = 'penny'
            
            elif pushed_key == '1':
                # Add a nickel to the total number of nickels
                coins_entered = 'nickel'
            
            elif pushed_key == '2':
                # Add a dime to the total number of dimes
                coins_entered = 'dime'
            
            elif pushed_key == '3':
                # Add a quarter to the total number of quarters
                coins_entered = 'quarter'
            
            elif pushed_key == '4':
                # Add a dollar to the total number of dollars
                coins_entered = 'one dollar bill'
            
            elif pushed_key == '5':
                # Add a five dollar bill to the total number of five dollar bills
                coins_entered = 'five dollar bill'
            
            elif pushed_key == '6':
                # Add a ten dollar bill to the total number of ten dollar bills
                coins_entered = 'ten dollar bill'
                
            elif pushed_key == '7':
                # Add a twenty dollar bill to the total number of twenty dollar bills
                coins_entered = 'twenty dollar bill'
                
            elif pushed_key == 'e':
                eject_button = True
            
            elif pushed_key == 'c':
                selection = 'Cuke'
            
            elif pushed_key == 'p':
                selection = 'Popsi'
            
            elif pushed_key == 's':
                selection = 'Spryte'
            
            elif pushed_key == 'd':
                selection = 'Dr. Pupper'
                
            else:
                print('ERROR: INPUT IS NOT VALID.')
            pushed_key = None

        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
    except KeyboardInterrupt:
        break

print ("Control-C has been pressed, so it's time to exit.")
    #keyboard.unhook_all ()