"""
@file       main.py

@brief      The mainscript that collects step response data and communicates with the computer-side script
            
@details    This is the mainscript for the Nucleo-side of the code. It communicates
            with the computer-side script via a shared serial port. If the 
            command 'g' is recognized from the serial port, this script will
            allow for data collection of the step response. The user can then
            briefly press the user button (connected to PC13) and have its step
            response recorded and sent back to the computer-side.

@n          This script constantly runs on the Nucleo, running a three-state
            finite-state-machine (FSM). In State 1, the FSM awaits a command
            from the computer-side script. If a 'g' is received, the FSM
            transitions to State 2. In State 2, the FSM awaits a button press
            from the user. If the user presses the button, an external interrupt
            is triggered and the step response is recorded from the analog-to-digital
            reader (ADC). The FSM then transitions to State 3, where the data
            is turned into a string and sent through the serial port. The FSM
            returns to State 1 and awaits another command.
            
@n          It's worth noting that this file should be named 'main.py' when it
            is on the Nucleo. With this name, the Nucleo will automatically
            run the script on startup, which is the only way this system will
            work.
            
@n          In this version, the analog pin is A1, which is connected via a jumper
            to the user button pin, PC13.
            
@author     Kyle Ladtkow

@date       February 2, 2021
"""

from pyb import UART
import array

## The array for time values from data collection
time = array.array('I', (0 for index in range(2500)))   #Initializes the time array with zeroes

## The array for voltage values from data collection
voltage = array.array('I', (0 for index in range(2500)))    #Initializes the voltage array with zeroes

## The array for time and corresponding voltage values from data collection
data = array.array('I', (0 for index in range(5000)))   #Initializes the data array with zeroes

## The frequency for which the timer will run at, in Hz
frequency = 2*(10**4)   #Initializes the frequency in Hz

## The timer object that the ADC reader will use to collect data
tim = pyb.Timer(2, freq = frequency)    #Creates the timer object for Timer 2, which is connected to analog pin A1

## The analog-to-digital converter object
adc = pyb.ADC(pyb.Pin.board.PA1)    #Creates the ADC object for analog pin A1

def step_response(pin):
    '''
    @brief      A function that performs an interrupt service routine (ISR) for the step response
    @details    When this function is called, it calls the global variable for
                voltage values and assigns it step response values.
    '''
    global voltage     #Calls the global variable for voltage
    adc.read_timed(voltage, tim) #Records the step response and assigns it to the voltage array

## The pyb object representing the external interrupt protocol
extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, step_response)  #Creates and defines the external interrupt protocols, including what edge to trigger on, what pin to observe, and what ISR to call

## UART Serial Object, the serial object that the data user interface task will use to communicate with the PC.
myuart = UART(2)    #Creates the serial object

## The variable representing what command has been sent via serial port
command = None      #Initializes the command variable

## The variable representing what state the FSM is in
state = 0       #Initializes the FSM's state as State 0

## Run the FSM on repeat
while True:
    if state == 0:
        ## State 0: Initialization
        # In this state, the FSM immediately moves on to State 1. Normally, this
        # state might have some welcome statement or initialization. I've left
        # this here in case I need to add something like that at some point.
        
        state = 1   #Moves the FSM to State 1
    
    elif state == 1:
        ## State 1: Wait for Command
        # In this state, the FSM constantly checks the serial port to see if
        # anything has been sent by the computer-side script. If a command is
        # received, the FSM checks if it is 'g'. If it is, the FSM moves to 
        # State 2. Otherwise, it discards the command and waits for the next one.
        
        ## If anything is read from the serial port, the variable command becomes what has been read
        if myuart.any():
            command = myuart.readchar()     #Sets the variable command as what has been read from the serial port
        
        ## If the command is 'g', the FSM moves to State 2 and resets the command variable
        if command == 103:
            command = None      #Resets the command variable
            state = 2       #Moves the FSM to State 2
        ## If the command isn't 'g', the FSM discards the command and goes back to waiting for another
        elif (command != None) and (command != 103):
            command = None      #Resets the command variable
            
    elif state == 2:
        ## State 2: Data Collection
        # In this state, the FSM is primed to collect data from the step response.
        # If the external interrupt triggers (i.e., the user quickly presses 
        # the button), the voltage array will take on all recorded voltage values
        # from that response. It then creates the time and total data array and
        # transitions to State 3 for data transmission.
        
        ## If the voltage array has changed (which it only will if the external interrupt has been triggered), the time array will be created. The time and voltage arrays will be combined into one data array.
        if (voltage != array.array('I', (0 for index in range(2500)))):
            ## For every voltage value, a corresponding time value is put into the time array
            for i in range(0,len(time)):
                time[i] = int(((10**6)*i)/frequency)    #Creates a time value based on which voltage value the loop is on and the frequency
            
            ## The indexing variable for which element of the data array the loop is on
            j = 0   #Initializes the indexing variable for the data array
            
            ## The indexing variable for which element of the time & voltage arrays the loop is on
            k = 0   #Initializes the indexing variable for the time & voltage arrays
            
            ## For every element of the data array, add the corresponding time and voltage values
            while (j < (len(data)-1)):
                data[j] = time[k]   #Adds the corresponding time value to the data array
                data[j+1] = voltage[k]  #Adds the corresponding voltage value to the data array
                j += 2      #Moves the data array index up to the next data pair
                k += 1      #Moves the time & voltage index up to the next value
            
            state = 3   #Moves the FSM to State 3
    
    elif state == 3:
        ## State 3: Data Transmission
        # In this state, the data collected in State 2 is compiled into a long 
        # string with predefined stop characters. This string is sent to the
        # serial port, where it will be picked up by the computer-side script.
        # The FSM then moves back to State 1, where it will wait for another
        # command.
        
        ## The indexing variable for which data point the compiler is on
        i = 0       #Initializes the compiler index variable
        
        ## For every data point, the time and voltage values are turned into a string of the form 'time,voltage&' and sent to the serial port.
        while (i < len(data)):
            ## The placeholder variable representing the compiled string for each data point
            printstr = str(data[i]) + ',' + str(data[i+1]) + '&'    #Compiles the desired data point into a string with stop characters ',' and '&'
            myuart.write(printstr.encode('ascii'))      #Writes the compiled string into the shared serial port
            i += 2      #Moves the compiler index variable to the next data point

        state = 1   #Moves the FSM back to State 1
            
        