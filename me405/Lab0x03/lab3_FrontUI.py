"""
@file       lab3_FrontUI.py

@brief      A mainscript that takes input from the user, collects data from
            the Nucleo, and plots that data.

@details    This script serves as the communication between the computer and
            the Nucleo. When run, it asks the user for input. If the input is
            the letter 'g', it sends that 'g' to the serial port, where it 
            will be passed into the Nucleo. It will then wait for data to be
            put into the serial window by the Nucleo, at which point it will
            process it. After processing, it will place the data into a .csv
            file and plot the step response.

@n          Because the communication between the PC and the Nucleo is through
            the serial port, the way data is sent through is via a string.
            The Nucleo encodes the data into a string with specific stop
            characters. This script reads that string and, using those same
            stop characters, it determines which data is time and which is voltage.

@n          This mainscript is a modified version of a mainscript I developed
            for Lab 0x04 in ME 305. For more information on that, please visit
            my Lab 0x04 page on my ME 305 portfolio.
@author     Kyle Ladtkow

@date       February 2, 2021
"""
import serial
import matplotlib.pyplot as plt
import numpy as np
import keyboard
import csv

## The serial object for the shared serial port
ser = serial.Serial(port='COM8',baudrate=115273,timeout=1)      #Initializes the serial object

## The list that will hold timestamp values from the Nucleo's data collection
time = []   #Initializes the time list

## The list that will hold voltage values from the Nucleo's data collection
voltage = []    #Initializes the voltage list

## A variable that represents whether the data processing has stopped
dataStop = 0    #Initializes the dataStop variable

## A variable that represents whether the 'g' character has been sent to the Nucleo
charSent = 0    #Initializes the charSent variable

## A variable that represents whether this script should stop or not
endScript = 0   #Initializes the endScript variable

## A variable that will take on whatever key has been pressed
pushed_key = None   #Initializes the pushed_key variable

def on_keypress (thing):
    """ 
    @brief      On a keypress, this function interrupts the code to log that key
    @author     John Ridgely
    @date       January 11, 2021
    """
    global pushed_key   #Calls the global pushed_key variable

    pushed_key = thing.name     #Sets the pushed_key variable to whatever has just been pressed

keyboard.on_press (on_keypress)  # Sets callback

def sendChar(inv):
    '''
    @brief      Sends a command to the Nucleo.
        
    @details    This function encodes a character into ASCII and sends it into
                the shared serial port. The Nucleo will then pick up this 
                character and process it.
    '''
    ser.write(str(inv).encode('ascii'))     #Encodes a character into ASCII and sends it to the serial port

def readSerial():
    '''
    @brief      Reads the serial port for data.
        
    @details    This function checks the serial port for if any data has been
                sent by the Nucleo. If data has been sent by the Nucleo in the
                form of a string, it will return that data. If the serial port
                still has a residual 'g', it will eliminate it before
                passing the data on.
    '''
    ## A variable representing what has been put into the serial port
    read = ser.readline().decode('ascii')   #Reads the serial port
    
    ## If what has been put into the serial port is 'g', remove the 'g' so that the data processor doesn't accidentally take it in
    if (read == 'g'):
        read = read.replace('g','')     #Remove any 'g' from the serial read
        return read     #Pass on the serial read
    
    ## If there is no 'g' in the serial port, pass on what has been written into the serial port
    else:
        return read     #Pass on the serial read

## When the endScript variable isn't active, this script runs, checking the serial port and processing data if it comes in
while (endScript != 1):
    ## If a key has been pressed, this block will check if that key is 'g' and send it to the serial port if it is.
    if pushed_key:
        if pushed_key == 'g':
            print('Nucleo is now waiting for a button press.')
            sendChar(pushed_key)    #Sends character encoded to ASCII to the Nucleo
            charSent = 1    #Sets the charSent variable to 1, denoting that a character has been sent to the Nucleo
        pushed_key = None   #Clears the pushed_key variable
    
    ## If a character has been sent to the Nucleo and data hasn't been processed
    if (dataStop != 1) and charSent == 1:
        ## The variable representing what has been read from the serial port
        data = readSerial()     #Constantly reads the serial for data from the Nucleo
        
        #If any data has been received from the Nucleo, data processing will begin
        if (data != ''):
            print('Data received, processing now...')
            ## Indexing variable for converting the string to usable data, represents the number of time-voltage pairs
            p = 0   #Initializes the index variable
            ## Indexing variable for converting the string to usable data, represents the number of data points (both time and voltage)
            n = 0   #Initializes the index variable
            
            ## For every data pair, pick through the characters of the string and extract the data, using stop characters ',' and '&' to denote time to voltage and voltage to time, respectively
            while (p != data.count('&')-1):
                ## Placeholder string for time values
                phtime = ''     #Initializes the placeholder time string
                ## Placeholder string for voltage values
                phvol = ''      #Initializes the placeholder voltage string
                
                ## For every character of one data value that isn't a ',', add that character to the placeholder time 
                while (data[n] != ','):
                    phtime += data[n]   #Add the character to the placeholder time string
                    n += 1      #Index to the next character
                n += 1      #Index to the next data value
                time.append(int(phtime)/(1*(10**6)))    #Add the integer version of the placeholder time string in seconds
                phtime = ''     #Clear the placeholder time string
                
                ## For every character of one data value that isn't a '&', add that character to the placeholder voltage 
                while (data[n] != '&'):
                    phvol += data[n]    #Add the character to the placeholder voltage string
                    n += 1      #Index to the next character
                n += 1      #Index to the next data value
                p += 1      #Index to the next data point
                voltage.append(int(int(phvol)*((3.3*1000)/4095)))     #Add the integer version of the placeholder voltage string in millivolts
                phvol = ''      #Clear the placeholder voltage string
            
            ## Write the extracted data to a .csv file 
            with open('stepresponse.csv', 'w') as csvfile:  
                ## A CSV writer object, coded for Excel format
                csvwriter = csv.writer(csvfile, dialect = 'excel')  #Creates the csvwriter object  
                ## A list object that will hold all of the extracted data in rows
                csvlist = []    # Initializes the csvlist object
                
                ## For every data point, write the time and voltage for each point as a new row in the csvlist object
                for row in range(0,len(voltage)):
                    csvlist.append([time[row], voltage[row]])   #Writes the data point as a new row
                
                csvwriter.writerows(csvlist)    #Writes the data in rows to the .csv file
   
            dataStop = 1    #Sets the dataStop variable to 1, telling the script to stop processing data and start plotting
    
    ## If the script has stopped processing data, it will plot it
    elif (dataStop == 1):
        fig, ax = plt.subplots()    #Initializes the plot figure
        ax.plot(time,voltage)   #Plots the time and voltage values from the extracted data
        ax.set_xlabel('Time [s]')   #Labels the x-axis
        ax.set_ylabel('Voltage [mV]')    #Labels the y-axis
        ax.set_title("Analog Step Response of the User Button") #Labels the plot
        ser.close()     #Closes the shared serial port on the computer side
        endScript = 1   #Sets the endScript variable to 1, telling the script to stop
        print('Data has been plotted.')
        pass
    
    else:
        pass