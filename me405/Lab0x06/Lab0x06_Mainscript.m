%% Lab 0x06
% ME 405
% Kyle Ladtkow

%% Housekeeping
clc;
clear all;
close all;

%% Part 1
syms x xdot T_x theta_y theta_doty r_m l_r r_B r_G l_P r_P r_C m_B m_P I_P b g I_B

%Matrix M
M_11 = -((m_B*(r_B^2))+(m_B*r_C*r_B)+I_B)/r_B;
M_12 = -((I_B*r_B)+(I_P*r_B)+(m_B*(r_B^3))+(m_B*r_B*(r_C^2))+(2*m_B*r_C*(r_B^2))+(m_P*r_B*(r_G^2))+(m_B*r_B*(x^2)))/r_B;
M_21 = -((m_B*(r_B^2))+I_B)/r_B;
M_22 = -((m_B*(r_B^3))+(m_B*r_C*(r_B^2))+(I_B*r_B))/r_B;

M = [M_11, M_12; M_21, M_22];

%Matrix f
f_1 = (b*theta_doty)-(g*m_B*(((r_B+r_C)*sin(theta_y))+(x*cos(theta_y))))+((l_P/r_m)*T_x)+(2*m_B*theta_doty*x*xdot)-(g*m_P*r_G*sin(theta_y));
f_2 = (-m_B*r_B*x*(theta_doty^2))-(g*m_B*r_B*sin(theta_y));

f = [f_1;f_2];

% Uncouple the system
q_dd = M\f;

% State Space Matrices
x_ss = [x; theta_y; xdot; theta_doty];
x_ssdot = [xdot; theta_doty; q_dd(1,1); q_dd(2,1)];

% Jacobian Linearizations
J_x = jacobian(x_ssdot, x_ss);
J_u = jacobian(x_ssdot, T_x);

% Substitute constants for symbolic variables
r_m = 60/1000; %[m], radius of lever arm
l_r = 50/1000; %[m], length of push rod
r_B = 10.5/1000; %[m], radius of ball
r_G = 42/1000; %[m], vertical distance from U-Joint to CG of platform
l_P = 110/1000; %[m], horizontal distance from U-Joint to push-rod pivot
r_P = 32.5/1000; %[m], vertical distance from U-Joint to push-rod pivot
r_C = 50/1000; %[m], vertical distance from U-Joint to platform surface
m_B = 30/1000; %[kg], mass of ball
m_P = 400/1000; %[kg], mass of platform
I_P = (1.88e6)/(1000^3); %[kg*m^2], moment of inertia of platform (about horizontal axis through CG)
b = 10/1000; %[N*m*s/rad]
g = 9.81; %[m/s^2)
I_B = (2/5)*m_B*(r_B^2); %[kg*m^2], moment of inertia of ball around CG

% Define the input variables to complete state-space linearization
x = 0; %[m], position of the ball
xdot = 0; %[m/s], velocity of the ball
theta_y = 0; %[rad], angle of the platform
theta_doty = 0; %[rad/s], angular velocity of the platform
T_x = 0; %[N-m], torque from the motor

% Solve for A and B
A = double(subs(J_x));
B = double(subs(J_u));

%% Part 2 & 3
% Initial Conditions for Cases 1-4

% Case 1: Ball is initially at rest, platform is level, platform is at
% rest, no torque input from the motor
init_x_c1 = [0;0;0;0]; %initial state matrix for x
T_c1 = [0]; %[N-m-s], torque input by the motor
t_s_c1 = 1; %[s], length of the simulation

% Case 2: Ball is initially at rest, platform is level, ball is offset
% horizontally from the CG by 5cm, no torque input from the motor
init_x_c2 = [5/100;0;0;0]; %initial state matrix for x
T_c2 = [0]; %[N-m-s], torque input by the motor
t_s_c2 = 0.4; %[s], length of the simulation

% Case 3: Ball is initially at rest, platform is inclined at 5 degrees, no
% torque input from the motor
init_x_c3 = [0;(5*pi)/180;0;0]; %initial state matrix for x
T_c3 = [0]; %[N-m-s], torque input by the motor
t_s_c3 = 0.4; %[s], length of the simulation

% Case 4: Ball is initially at rest, platform is level, impulse of 1 mN-m-s
% applied by the motor
init_x_c4 = [0;0;0;0]; %initial state matrix for x
T_c4 = [1/1000]; %[N-m-s], torque input from the motor
t_s_c4 = 0.4; %[s], length of the simulation


% Pull from simulation
simulation2 = sim('Lab0x06_Sim_OL.slx');

t2 = simulation2.tout;
x_c1 = simulation2.x_c1;
x_c2 = simulation2.x_c2;
x_c3 = simulation2.x_c3;
x_c4 = simulation2.x_c4;

% Case 1 Data
t_c1 = t2;
x_pos_c1 = x_c1(:,1);
theta_c1 = x_c1(:,2);
x_dot_c1 = x_c1(:,3);
theta_dot_c1 = x_c1(:,4);

figure(1)
subplot(2,2,1)
plot(t_c1, x_pos_c1(1:length(t_c1)))
grid minor
xlabel('Time (s)')
ylabel('Position (m)')

subplot(2,2,2)
plot(t_c1, theta_c1(1:length(t_c1)))
grid minor
xlabel('Time (s)')
ylabel('Angle (rad)')

subplot(2,2,3)
plot(t_c1, x_dot_c1(1:length(t_c1)))
grid minor
xlabel('Time (s)')
ylabel('Velocity (m/s)')

subplot(2,2,4)
plot(t_c1, theta_dot_c1(1:length(t_c1)))
grid minor
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')


% Case 2 Data
t_c2 = t2(t2<=t_s_c2);
x_pos_c2 = x_c2(:,1);
theta_c2 = x_c2(:,2);
x_dot_c2 = x_c2(:,3);
theta_dot_c2 = x_c2(:,4);

figure(2)
subplot(2,2,1)
plot(t_c2, x_pos_c2(1:length(t_c2)))
grid minor
xlabel('Time (s)')
ylabel('Position (m)')

subplot(2,2,2)
plot(t_c2, theta_c2(1:length(t_c2)))
grid minor
xlabel('Time (s)')
ylabel('Angle (rad)')

subplot(2,2,3)
plot(t_c2, x_dot_c2(1:length(t_c2)))
grid minor
xlabel('Time (s)')
ylabel('Velocity (m/s)')

subplot(2,2,4)
plot(t_c2, theta_dot_c2(1:length(t_c2)))
grid minor
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')


% Case 3 Data
t_c3 = t2(t2<=t_s_c3);
x_pos_c3 = x_c3(:,1);
theta_c3 = x_c3(:,2);
x_dot_c3 = x_c3(:,3);
theta_dot_c3 = x_c3(:,4);

figure(3)
subplot(2,2,1)
plot(t_c3, x_pos_c3(1:length(t_c3)))
grid minor
xlabel('Time (s)')
ylabel('Position (m)')

subplot(2,2,2)
plot(t_c3, theta_c3(1:length(t_c3)))
grid minor
xlabel('Time (s)')
ylabel('Angle (rad)')

subplot(2,2,3)
plot(t_c3, x_dot_c3(1:length(t_c3)))
grid minor
xlabel('Time (s)')
ylabel('Velocity (m/s)')

subplot(2,2,4)
plot(t_c3, theta_dot_c3(1:length(t_c3)))
grid minor
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')


% Case 4 Data
t_c4 = t2(t2<=t_s_c4);
x_pos_c4 = x_c4(:,1);
theta_c4 = x_c4(:,2);
x_dot_c4 = x_c4(:,3);
theta_dot_c4 = x_c4(:,4);

figure(4)
subplot(2,2,1)
plot(t_c4, x_pos_c4(1:length(t_c4)))
grid minor
xlabel('Time (s)')
ylabel('Position (m)')

subplot(2,2,2)
plot(t_c4, theta_c4(1:length(t_c4)))
grid minor
xlabel('Time (s)')
ylabel('Angle (rad)')

subplot(2,2,3)
plot(t_c4, x_dot_c4(1:length(t_c4)))
grid minor
xlabel('Time (s)')
ylabel('Velocity (m/s)')

subplot(2,2,4)
plot(t_c4, theta_dot_c4(1:length(t_c4)))
grid minor
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')

%% Part 4
% New Gain Matrix
K = [-0.3, -0.2, -0.05, -0.02];
simulation4 = sim('Lab0x06_Sim_CL.slx');

t4 = simulation4.tout;
x_CL = simulation4.x_CL;

x_pos_CL = x_CL(:,1);
theta_CL = x_CL(:,2);
x_dot_CL = x_CL(:,3);
theta_dot_CL = x_CL(:,4);

figure(5)
subplot(2,2,1)
plot(t4, x_pos_CL)
grid minor
xlabel('Time (s)')
ylabel('Position (m)')

subplot(2,2,2)
plot(t4, theta_CL)
grid minor
xlabel('Time (s)')
ylabel('Angle (rad)')

subplot(2,2,3)
plot(t4, x_dot_CL)
grid minor
xlabel('Time (s)')
ylabel('Velocity (m/s)')

subplot(2,2,4)
plot(t4, theta_dot_CL)
grid minor
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')