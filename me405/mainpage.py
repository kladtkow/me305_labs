'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
@details This website includes detailed documentation for all of the projects I completed in ME 405.
@author Kyle Ladtkow
@date January 5, 2021
'''