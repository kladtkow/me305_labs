'''
@file mcp9808.py

@brief The mcp9808 driver uses an I2C object to read data from the temperature sensor

@details The mcp9808 driver contains methods to initialize the object, check to make sure there is a device connected to the supplied
         address, and measure the temperature in celsius and fahrenheit.

@author Daniel Coleman
@author Kyle Ladtkow

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
import pyb

class mcp9808:
    
    def __init__(self, address):
        '''
            @brief Initialized the mcp9808 object
            @param address Address of the desired I2C device
        '''
        ## I2C object for the mcp9808
        self.i2c1 = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 100000)
        
        ## An integer value to store the address
        self.address = address
        
        ## A list to store a list of additional found addresses
        self.foundAddresses = []
        
        ## A byte array to store the output from the temperature sensor
        self.data = bytearray(3)
        
        ## A boolean to check if the address was found
        self.addressFound = False
        
    def check(self):
        '''
            @brief      Checks to make sure the supplied address exists
            @details    This method is meant to ensure that the given I2C address
                        is correct. It scans and records all available I2C 
                        addresses on the bus. If one of these addresses matches
                        the supplied address, it sets the boolean addressFound
                        variable to True.
        '''
        
        self.foundAddresses = self.i2c1.scan()  #Scans the I2C bus for available addresses
        
        ## If any addresses have been found, this loop will check if any of the
        #addresses match the given address.
        if self.foundAddresses != []:
            ## Cycle through the found addresses, checking for a match
            for device in range(0,len(self.foundAddresses)):
                ## If a match is found, set the addressFound boolean to True and break
                if self.foundAddresses[device] == self.address:
                    print('Address is correct!')
                    self.addressFound = True    #Sets the addressFound variable to True
                    break
                ## If a match isn't found, print an error and try again
                else:
                    print('Address is incorrect')
                    
    def celsius(self):
        '''
            @brief      Returns the temperature in degrees Celsius
            @details    This function reads and processes the temperature data 
                        from the I2C sensor. It outputs the data in degrees 
                        Celsius.
            
        '''
        ## If the address supplied has been found, read the temperature data
        if self.addressFound == True:
            self.i2c1.mem_read(self.data, self.address, 5, timeout=5000, addr_size = 8)    #Grabs the data bit from the sensor as a base 10 integer
            
            ## Processes the data into Celsius, as given by the datasheet
            if self.data[2] & 0x10 == 0x10:
                self.data[2] = self.data[2] & 0x0F
                return (self.data[2] * 16 + self.data[1] / 16.0) - 256
            return self.data[2] * 16 + self.data[1] / 16.0      #Returns the temperature in Celsius
        ## If the address has not been found, print an error
        else:
            print('ERROR! Device has not been found')
            
    def fahrenheit(self):
        '''
            @brief      Returns the temperature in degrees Fahrenheit
            details     This function calls upon the Celsius method to read
                        and process temperature data from the I2C sensor. It 
                        outputs the data in degrees Fahrenheit.
        '''
        ## If the address supplied has been found, read the temperature data        
        if self.addressFound == True:
            return self.celsius()*9/5+32    #Use the celsius function to grab the temperature data and convert it to Fahrenheit

if __name__ == "__main__":
    sensor = mcp9808(0x18)      #Initializes the sensor object at address 0x18
    addresses = sensor.check()  #Checks that the given address works with our I2C sensor  
    while True:
        ## For as long as the program runs, try the following block
        try:
            print(sensor.celsius())     #Print the current temperature in Celsius
            pyb.delay(1000)     #Delay the board by 1 second
        
        ## If CTRL+C is detected, break the code
        except KeyboardInterrupt:
            break
