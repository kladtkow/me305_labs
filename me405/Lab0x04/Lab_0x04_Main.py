'''
@file Lab_0x04_Main.py

@brief This main file runs on the nucleo collecting temperature data every 60 seconds and saves it to a csv file.

@author Daniel Coleman
@author Kyle Ladtkow

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
import utime
from mcp9808 import mcp9808

## ADC object on pin A0
adcall = pyb.ADCAll(12, 0x70000)

## Object for the mcp9808 temperature sensor
mcp = mcp9808(0x18)

# Check the address is correct
mcp.check()

## An Array to hold time stamps
Time = []

## An Array to hold Nucleo temperature readings
TempSTM = []

## An Array to hold mcp9808 temperature readings
TempMCP = []

## Start time in ms
start_Time = utime.ticks_ms()

## Step time in ms
step_Time = 60000

## Indexing variable
i = 1

while True:
    ## As long as the code is running, try the following block of code
    try:
            
            # Add time stamp to data
            Time.append(utime.ticks_diff(utime.ticks_ms(),start_Time)/1000)
            # Read vref to set core temp
            a = adcall.read_core_vref()
            # Read and append nucleo temp
            TempSTM.append(adcall.read_core_temp())
            # Read and append mcp temp
            TempMCP.append(mcp.celsius())
            # Pass given step time
            utime.sleep_ms(step_Time)
    
    ## If CTRL+C is detected, break the code
    except KeyboardInterrupt:
        print('Data Collection Stopped')
        break

## Save data to a csv file    
with open ("Temperatures.csv", "w") as a_file:
    ## For every data point, write the time, STM temperature, and MCP temperature to the .csv file
    for line in range(len(Time)):
        a_file.write (str(Time[line]) + ',' + str(TempSTM[line])+ ',' + str(TempMCP[line]) + '\n')

print ("The file has by now automatically been closed.")


